--
--    Copyright 2010-2016 the original author or authors.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.
--

-- // create reservations table
-- Migration SQL that makes the change goes here.

CREATE TABLE reservations
(
   id bigserial NOT NULL,
   user_id bigint,
   restaurant_id bigint,
   note TEXT,
   capacity smallint NOT NULL DEFAULT 1,
   is_deleted boolean DEFAULT false,
   reservation_at timestamp NOT NULL,
   status smallint NOT NULL check (status in(1,2,3,4,5)),
   created_at timestamp without time zone NOT NULL DEFAULT LOCALTIMESTAMP, 
   updated_at timestamp without time zone, 
   CONSTRAINT "PK_RESERVATIONS" PRIMARY KEY (id),
   CONSTRAINT "FK_RESERVATIONS_USER_ID" FOREIGN KEY (user_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
   CONSTRAINT "FK_RESERVATIONS_RESTAURANT_ID" FOREIGN KEY (restaurant_id) REFERENCES restaurants (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
);

comment on column reservations.status is '1=pending, 2=confirmed, 3=canceled, 4=completed, 5=rejected';
CREATE INDEX idx_reservations_id ON reservations (id ASC NULLS LAST);
CREATE INDEX idx_reservations_user_id ON reservations (user_id ASC NULLS LAST);
CREATE INDEX idx_reservations_restaurants_id ON reservations (restaurant_id ASC NULLS LAST);

-- //@UNDO
-- SQL to undo the change goes here.

DROP INDEX idx_reservations_restaurants_id;
DROP INDEX idx_reservations_user_id;
DROP INDEX idx_reservations_id;

DROP TABLE reservations;


