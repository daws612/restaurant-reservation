--
--    Copyright 2010-2016 the original author or authors.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.
--

-- // create restaurants table
-- Migration SQL that makes the change goes here.

CREATE TABLE restaurants
(
   id bigserial NOT NULL,
   user_id bigint,
   name character varying(128) NOT NULL, 
   main_phone character varying(32) NULL,
   address character varying(255),
   description TEXT NULL,
   latitude REAL,
   longitude REAL,
   capacity smallint NOT NULL DEFAULT 1,
   is_deleted boolean DEFAULT false,
   created_at timestamp without time zone NOT NULL DEFAULT LOCALTIMESTAMP, 
   updated_at timestamp without time zone, 
   CONSTRAINT "PK_RESTAURANTS" PRIMARY KEY (id),
   CONSTRAINT "FK_RESTAURANTS_USER_ID" FOREIGN KEY (user_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE RESTRICT
) 
WITH (
  OIDS = FALSE
);

CREATE INDEX idx_restaurants_id ON restaurants (id ASC NULLS LAST);
CREATE INDEX idx_restaurants_user_id ON restaurants (user_id ASC NULLS LAST);

-- //@UNDO
-- SQL to undo the change goes here.

DROP INDEX idx_restaurants_id;
DROP INDEX idx_restaurants_user_id;
DROP TABLE restaurants;
