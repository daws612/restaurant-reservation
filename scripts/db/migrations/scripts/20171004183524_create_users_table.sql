--
--    Copyright 2010-2016 the original author or authors.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.
--

-- // create users table
-- Migration SQL that makes the change goes here.

CREATE TABLE users
(
   id bigserial NOT NULL, 
   email character varying(128) NOT NULL, 
   password character varying(128),
   first_name character varying(64),
   last_name character varying(64),
   main_phone character varying(32) NULL,
   user_type smallint NOT NULL check (user_type in(1,2)),
   created_at timestamp without time zone NOT NULL DEFAULT LOCALTIMESTAMP, 
   updated_at timestamp without time zone, 
   CONSTRAINT "PK_USERS" PRIMARY KEY (id),
   CONSTRAINT "UNIQUE_USER_EMAIL" UNIQUE(email)
) 
WITH (
  OIDS = FALSE
);

comment on column users.user_type is '1=customer, 2=owner';
CREATE INDEX idx_users_id ON users (id ASC NULLS LAST);
CREATE INDEX idx_users_email ON users (email ASC NULLS LAST);

-- //@UNDO
-- SQL to undo the change goes here.

DROP INDEX idx_users_id;
DROP INDEX idx_users_email;

DROP TABLE users;
