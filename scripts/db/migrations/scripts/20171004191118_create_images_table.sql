--
--    Copyright 2010-2016 the original author or authors.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.
--

-- // create images table
-- Migration SQL that makes the change goes here.

CREATE TABLE images
(
   id bigserial NOT NULL,
   restaurant_id bigint,
   image_url character varying (512),
   width smallint,
   height smallint,
   created_at timestamp without time zone NOT NULL DEFAULT LOCALTIMESTAMP, 
   updated_at timestamp without time zone, 
   CONSTRAINT "PK_IMAGES" PRIMARY KEY (id),
   CONSTRAINT "FK_IMAGES_RESTAURANT_ID" FOREIGN KEY (restaurant_id) REFERENCES restaurants (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
);

CREATE INDEX idx_images_id ON images (id ASC NULLS LAST);
CREATE INDEX idx_images_restaurants_id ON images (restaurant_id ASC NULLS LAST);

-- //@UNDO
-- SQL to undo the change goes here.

DROP INDEX idx_images_restaurants_id;
DROP INDEX idx_images_id;

DROP TABLE images;

