--
--    Copyright 2010-2016 the original author or authors.
--
--    Licensed under the Apache License, Version 2.0 (the "License");
--    you may not use this file except in compliance with the License.
--    You may obtain a copy of the License at
--
--       http://www.apache.org/licenses/LICENSE-2.0
--
--    Unless required by applicable law or agreed to in writing, software
--    distributed under the License is distributed on an "AS IS" BASIS,
--    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
--    See the License for the specific language governing permissions and
--    limitations under the License.
--

-- // create reviews table
-- Migration SQL that makes the change goes here.

CREATE TABLE reviews
(
   id bigserial NOT NULL,
   user_id bigint,
   restaurant_id bigint,
   review_text TEXT,
   rating REAL,
   created_at timestamp without time zone NOT NULL DEFAULT LOCALTIMESTAMP, 
   updated_at timestamp without time zone, 
   CONSTRAINT "PK_REVIEWS" PRIMARY KEY (id),
   CONSTRAINT "FK_REVIEWS_USER_ID" FOREIGN KEY (user_id) REFERENCES users (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
   CONSTRAINT "FK_REVIEWS_RESTAURANT_ID" FOREIGN KEY (restaurant_id) REFERENCES restaurants (id) ON UPDATE NO ACTION ON DELETE NO ACTION
) 
WITH (
  OIDS = FALSE
);

CREATE INDEX idx_reviews_id ON reviews (id ASC NULLS LAST);
CREATE INDEX idx_reviews_user_id ON reviews (user_id ASC NULLS LAST);
CREATE INDEX idx_reviews_restaurants_id ON reviews (restaurant_id ASC NULLS LAST);

-- //@UNDO
-- SQL to undo the change goes here.

DROP INDEX idx_reviews_restaurants_id;
DROP INDEX idx_reviews_user_id;
DROP INDEX idx_reviews_id;

DROP TABLE reviews;
