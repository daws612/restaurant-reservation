--users
INSERT INTO users( email, password, first_name, last_name, main_phone, user_type,  created_at) VALUES ( 'customer1@cytonn.com', 'customer1', 'CustomerName1', 'CustomerName1', '0708123456', 1, now());
INSERT INTO users( email, password, first_name, last_name, main_phone, user_type,  created_at) VALUES ( 'owner1@cytonn.com', 'owner1', 'OwnerName1', 'OwnerName1', '0712789456', 2, now());

--restaurants
INSERT INTO restaurants(user_id, name, main_phone, address, description, latitude, longitude, capacity, is_deleted, created_at) VALUES (2, 'Restaurant1', '0741369258', '123 Main Restaurant1 street', 'Small description for Restaurant1', -1.217345, 36.885464, 500, false, now());
INSERT INTO restaurants(user_id, name, main_phone, address, description, latitude, longitude, capacity, is_deleted, created_at) VALUES (2, 'Restaurant2', '0741369258', '456 Main Restaurant2 street', 'Small description for Restaurant2', -1.219952, 36.880035, 250, false, now());
INSERT INTO restaurants(user_id, name, main_phone, address, description, latitude, longitude, capacity, is_deleted, created_at) VALUES (2, 'Restaurant3', '0741369258', '789 Main Restaurant3 street', 'Small description for Restaurant3', -1.218128, 36.886312, 46, false, now());
INSERT INTO restaurants(user_id, name, main_phone, address, description, latitude, longitude, capacity, is_deleted, created_at) VALUES (2, 'Restaurant4', '0741369258', '045 Main Restaurant4 street', 'Small description for Restaurant4', -1.215297, 36.887545, 730, false, now());
INSERT INTO restaurants(user_id, name, main_phone, address, description, latitude, longitude, capacity, is_deleted, created_at) VALUES (2, 'Restaurant5', '0741369258', '630 Main Restaurant5 street', 'Small description for Restaurant5', -1.219576, 36.889005, 89, false, now());

INSERT INTO reservations( user_id, restaurant_id, note, capacity, is_deleted, reservation_at, status, created_at) VALUES (1, 1, 'note for resv user1 rest1', 2, false, now() + '3 days', 1, now());
INSERT INTO reservations( user_id, restaurant_id, note, capacity, is_deleted, reservation_at, status, created_at) VALUES (1, 2, 'note for resv user2 rest2', 3, false, now() + '1 days', 2, now());
INSERT INTO reservations( user_id, restaurant_id, note, capacity, is_deleted, reservation_at, status, created_at) VALUES (1, 3, 'note for resv user3 rest3', 4, false, now() + '6 days', 3, now());
INSERT INTO reservations( user_id, restaurant_id, note, capacity, is_deleted, reservation_at, status, created_at) VALUES (1, 4, 'note for resv user4 rest4', 5, false, now() + '8 days', 4, now());
INSERT INTO reservations( user_id, restaurant_id, note, capacity, is_deleted, reservation_at, status, created_at) VALUES (1, 5, 'note for resv user5 rest5', 6, false, now() + '15 days', 5, now());

INSERT INTO reviews(user_id, restaurant_id, review_text, rating, created_at) VALUES (1, 1, 'review for rest1 by user1', 1, now());
INSERT INTO reviews(user_id, restaurant_id, review_text, rating, created_at) VALUES (1, 2, 'review for rest2 by user1', 5, now());
INSERT INTO reviews(user_id, restaurant_id, review_text, rating, created_at) VALUES (1, 3, 'review for rest3 by user1', 4, now());
INSERT INTO reviews(user_id, restaurant_id, review_text, rating, created_at) VALUES (1, 2, 'review for rest2 by user1', 5, now());
INSERT INTO reviews(user_id, restaurant_id, review_text, rating, created_at) VALUES (1, 2, 'review for rest2 by user1', 3, now());
INSERT INTO reviews(user_id, restaurant_id, review_text, rating, created_at) VALUES (1, 2, 'review for rest2 by user1', 0, now());
