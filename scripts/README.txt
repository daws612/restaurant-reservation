In order to set up the server database:

Make sure you have postgresql installed in your machine.
Create a "restaurant_reservation_development" named database.

In \restaurant-reservation\scripts\db\migrations\environments\development.properties
	- set the username and password of the "restaurant_reservation_development" you created above
	- also make sure the host and port point to your postgres database instance

From cmd, cd into \restaurant-reservation\scripts\db
	- run the migrations using "mybatis-migrations\bin\migrate --path=migrations\ up"

Confirm that these tables have been created under the public schema

changelog
users
restaurants
reservations
reviews
images

For dummy data, run the insert statements in \restaurant-reservation\scripts\db\migrations\exports\dummy_data.sql