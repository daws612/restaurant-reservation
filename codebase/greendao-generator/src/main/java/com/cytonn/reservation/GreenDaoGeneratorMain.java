package com.cytonn.reservation;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Property;
import org.greenrobot.greendao.generator.Schema;
import org.greenrobot.greendao.generator.ToMany;


public class GreenDaoGeneratorMain {

    private static Entity appConfig;
	private static Entity user;
	private static Entity restaurant;
	private static Entity image;
	private static Entity reservation;
	private static Entity review;

	public static void main(String[] args) throws Exception {
		Schema schema = new Schema(1, "com.cytonn.reservation.db.models");
		schema.setDefaultJavaPackageDao("com.cytonn.reservation.db.dao");
		schema.setDefaultJavaPackageTest("com.cytonn.reservation.test.db.dao");
		schema.enableKeepSectionsByDefault();

		addUsers(schema);
		addRestaurant(schema);
		addImage(schema);
		addReservation(schema);
		addReview(schema);
		addAppConfig(schema);

		new DaoGenerator().generateAll(schema, "./src-gen");
	}

	private static void addUsers(Schema schema) {
		user = schema.addEntity("User");

		user.setTableName("users");

		user.addIdProperty().autoincrement();
		user.addStringProperty("firstName").columnType("VARCHAR(64)");
		user.addStringProperty("lastName").columnType("VARCHAR(64)");
        user.addStringProperty("email").columnType("VARCHAR(128)");
        user.addStringProperty("password");
        user.addStringProperty("phoneNumber").columnType("VARCHAR(32)");
		user.addIntProperty("userType").notNull();
		user.addDateProperty("createdAt");
		user.addDateProperty("updatedAt");

	}

	private static void addRestaurant(Schema schema) {
		restaurant = schema.addEntity("Restaurant");

		restaurant.setTableName("restaurants");

		restaurant.addIdProperty().autoincrement();
		restaurant.addStringProperty("name").columnType("VARCHAR(64)");
		restaurant.addStringProperty("phoneNumber").columnType("VARCHAR(32)");
		restaurant.addStringProperty("address");
		restaurant.addStringProperty("description");
		restaurant.addDoubleProperty("latitude").notNull();
		restaurant.addDoubleProperty("longitude").notNull();
		restaurant.addIntProperty("isDeleted");
		restaurant.addIntProperty("capacity");
		restaurant.addDateProperty("createdAt");
		restaurant.addDateProperty("updatedAt");
		
        Property userIdProperty = restaurant.addLongProperty("userId").getProperty();
        restaurant.addToOne(user, userIdProperty, "user");

	}

	private static void addImage(Schema schema) {
        image = schema.addEntity("Image");
        image.setTableName("images");
        image.addIdProperty().autoincrement();
        image.addIntProperty("width").notNull();
        image.addIntProperty("height");
        image.addStringProperty("imageUrl").columnType("VARCHAR(255)");
        image.addDateProperty("createdAt");
        image.addDateProperty("updatedAt");
        
        Property restaurantsIdProperty = image.addLongProperty("restaurantId").getProperty();
        image.addToOne(restaurant, restaurantsIdProperty, "restaurant");
        
        ToMany restToImages = restaurant.addToMany(image, restaurantsIdProperty);
        restToImages.setName("images");
    }

	private static void addReservation(Schema schema) {
        reservation = schema.addEntity("Reservation");
        reservation.setTableName("reservations");
        reservation.addIdProperty().autoincrement();
        reservation.addIntProperty("capacity");
        reservation.addIntProperty("status").notNull();
        reservation.addLongProperty("reservationAt");
        reservation.addStringProperty("note");
		reservation.addIntProperty("isDeleted");
        reservation.addDateProperty("createdAt");
        reservation.addDateProperty("updatedAt");
        
        Property restaurantsIdProperty = reservation.addLongProperty("restaurantId").getProperty();
        reservation.addToOne(restaurant, restaurantsIdProperty, "restaurant");
		
        Property userIdProperty = reservation.addLongProperty("userId").getProperty();
        reservation.addToOne(user, userIdProperty, "user");
        
        ToMany restToReservations = restaurant.addToMany(reservation, restaurantsIdProperty);
        restToReservations.setName("reservations");
        
        ToMany userToReservations = user.addToMany(reservation, userIdProperty);
        userToReservations.setName("reservations");
    }

	private static void addReview(Schema schema) {
        review = schema.addEntity("Review");
        review.setTableName("reviews");
        review.addIdProperty().autoincrement();
		review.addDoubleProperty("rating").notNull();
        review.addStringProperty("reviewText");
        review.addDateProperty("createdAt");
        review.addDateProperty("updatedAt");
        
        Property restaurantsIdProperty = review.addLongProperty("restaurantId").getProperty();
        review.addToOne(restaurant, restaurantsIdProperty, "restaurant");
		
        Property userIdProperty = review.addLongProperty("userId").getProperty();
        review.addToOne(user, userIdProperty, "user");
        
        ToMany restToReviews = restaurant.addToMany(review, restaurantsIdProperty);
        restToReviews.setName("reviews");
        
        ToMany userToReviews = user.addToMany(review, userIdProperty);
        userToReviews.setName("reviews");
    }
	
	private static void addAppConfig(Schema schema) {
        appConfig = schema.addEntity("AppConfig");
        appConfig.setTableName("app_config");
        appConfig.addIdProperty().autoincrement();
        appConfig.addIntProperty("dbSchemaVersion");
        appConfig.addStringProperty("staticImageCache");
        appConfig.addStringProperty("tempImageCache");
        Property currentUserId = appConfig.addLongProperty("currentUserId").getProperty();
        appConfig.addToOne(user, currentUserId, "currentUser");
        Property lastLoggedInUserId = appConfig.addLongProperty("lastLoggedInUserId").getProperty();
        appConfig.addToOne(user, lastLoggedInUserId, "lastLoggedInUser");
        appConfig.addDateProperty("createdAt");
        appConfig.addDateProperty("updatedAt");
    }
}
