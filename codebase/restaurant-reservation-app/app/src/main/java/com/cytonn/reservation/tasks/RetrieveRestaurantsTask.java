package com.cytonn.reservation.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.cytonn.reservation.RestaurantReservationApp;
import com.cytonn.reservation.db.dao.DaoSession;
import com.cytonn.reservation.db.dao.RestaurantDao;
import com.cytonn.reservation.db.models.Restaurant;
import com.cytonn.reservation.models.RestaurantItem;
import com.cytonn.reservation.shared.Constants;
import com.cytonn.reservation.utils.HttpHelper;
import com.google.android.gms.maps.model.LatLng;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by F'rdaws on 01-Oct-17.
 */

public class RetrieveRestaurantsTask extends AsyncTask<Object, Void, Void> {
    private final String TAG = this.getClass().getSimpleName();

    Callback callback;
    DaoSession daoSession;

    public RetrieveRestaurantsTask(Callback callback) {
        this.callback = callback;
        this.daoSession = RestaurantReservationApp.getDaoSessionInstance();
    }

    @Override
    protected Void doInBackground(Object... objects) {
        try {
            String restName = null;
            if (objects != null && objects.length > 0)
                restName = (String) objects[0];

            List<RestaurantItem> restaurantItems = null;

            if (restName != null && !restName.isEmpty())
                restaurantItems = searchRestaurants(restName);
            else
                restaurantItems = searchServer();

            if (restaurantItems != null && !restaurantItems.isEmpty())
                callback.finished(restaurantItems);
            else if (restaurantItems != null && !restaurantItems.isEmpty())
                callback.error("No restaurants found");

        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
            return null;
        }

        return null;
    }

    private List<RestaurantItem> searchRestaurants(String restName) {
        List<RestaurantItem> restaurantItemList = getLocallySavedRestaurants();
        List<RestaurantItem> resultItemList = new ArrayList<>();
        for (RestaurantItem item : restaurantItemList) {
            if (item.getName().toLowerCase().contains(restName.toLowerCase()))
                resultItemList.add(item);
        }
        return resultItemList;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    public interface Callback {
        void finished(List<RestaurantItem> restaurantItemList);

        void error(String message);
    }

    private List<RestaurantItem> getLocallySavedRestaurants() {
        List<RestaurantItem> restaurantItemList = new ArrayList<>();

        List<Restaurant> restaurantList = daoSession.getRestaurantDao().queryBuilder().orderDesc(RestaurantDao.Properties.Id).list();

        for (Restaurant restaurant : restaurantList) {
            restaurantItemList.add(convertToRestaurantItem(restaurant));
        }

        return restaurantItemList;
    }

    private RestaurantItem convertToRestaurantItem(Restaurant restaurant) {
        RestaurantItem item = new RestaurantItem(restaurant.getId(), restaurant.getName(), new LatLng(restaurant.getLatitude(), restaurant.getLongitude()));
        if (restaurant.getImages() != null && restaurant.getImages().size() > 0)
            item.setImageUrl(restaurant.getImages().get(0).getImageUrl());
        return item;
    }

    private List<RestaurantItem> searchServer() {
        try {

            HttpHelper httpHelper = new HttpHelper();

            List<NameValuePair> params = new ArrayList<NameValuePair>();
            byte[] bytes = httpHelper.sendGet(Constants.ServerEndPoints.GET_RESTAURANTS, params);
            String response = new String(bytes);
            Log.i(TAG, "Response from url: " + response);
            List<RestaurantItem> restaurants = convertToRestaurantItemObject(response);
            restaurants.addAll(getLocallySavedRestaurants());
            return restaurants;

        } catch (ConnectException ex) { //if server is not up, return the locally saved ones only
            ex.printStackTrace();
            return getLocallySavedRestaurants();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private List<RestaurantItem> convertToRestaurantItemObject(String response) {
        List<RestaurantItem> resList = new ArrayList<>();

        if (response != null) {
            try {
                JSONObject jsonObj = new JSONObject(response);
                JSONObject data = jsonObj.getJSONObject("data");

                // Getting JSON Array node
                JSONArray restaurants = data.getJSONArray("restList");

                // looping through All Contacts
                for (int i = 0; i < restaurants.length(); i++) {
                    JSONObject c = restaurants.getJSONObject(i);

                    Integer id = c.getInt("id");
                    Long userId = c.getLong("userId");
                    String name = c.getString("name");
                    String mainPhone = c.getString("mainPhone");
                    String address = c.getString("address");
                    String description = c.getString("description");
                    Double latitude = c.getDouble("latitude");
                    Double longitude = c.getDouble("longitude");
                    Integer capacity = c.getInt("capacity");
                    Boolean isDeleted = c.getBoolean("isDeleted");

                    Restaurant restaurant = new Restaurant();
                    restaurant.setMasterId(id);
                    restaurant.setName(name);
                    restaurant.setUserId(userId);
                    restaurant.setPhoneNumber(mainPhone);
                    restaurant.setAddress(address);
                    restaurant.setDescription(description);
                    restaurant.setLatitude(latitude);
                    restaurant.setLongitude(longitude);
                    restaurant.setCapacity(capacity);
                    if (isDeleted)
                        restaurant.setIsDeleted(1);
                    else
                        restaurant.setIsDeleted(0);

                    daoSession.getRestaurantDao().insert(restaurant);

                    resList.add(convertToRestaurantItem(restaurant));
                }
            } catch (final JSONException e) {
                Log.e(TAG, "Json parsing error: " + e.getMessage());
                return null;

            } catch (final Exception e) {
                Log.e(TAG, "Unexpected exception: " + e.getMessage());
                return null;

            }
        }
        return resList;
    }
}
