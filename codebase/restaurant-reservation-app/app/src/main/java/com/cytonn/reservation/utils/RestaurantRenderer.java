package com.cytonn.reservation.utils;

import android.content.Context;

import com.cytonn.reservation.models.RestaurantItem;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

/**
 * Created by Firdaws on 13-Jan-17.
 */
public class RestaurantRenderer extends DefaultClusterRenderer<RestaurantItem> {

    Context context;

    public RestaurantRenderer(Context context, GoogleMap map, ClusterManager<RestaurantItem> clusterManager) {
        super(context, map, clusterManager);
        this.context = context;
    }

    @Override
    protected void onBeforeClusterItemRendered(RestaurantItem item, MarkerOptions markerOptions) {
        super.onBeforeClusterItemRendered(item, markerOptions); //can be overridden to use custom markers

    }

    @Override
    protected void onClusterRendered(Cluster<RestaurantItem> cluster, Marker marker) {
        super.onClusterRendered(cluster, marker);

    }

    @Override
    protected void onClusterItemRendered(RestaurantItem clusterItem, Marker marker) {
        super.onClusterItemRendered(clusterItem, marker);

    }
}
