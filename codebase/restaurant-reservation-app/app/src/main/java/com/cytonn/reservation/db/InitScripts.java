package com.cytonn.reservation.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public abstract class InitScripts extends SQLiteOpenHelper {
    private final String TAG = this.getClass().getSimpleName();

    public InitScripts(Context context, String name, int version) {
        super(context, name, null, version);
    }

    String create_users = "CREATE TABLE users (_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, first_name TEXT,last_name TEXT, " +
            "email TEXT NOT NULL, password TEXT NOT NULL, phone_number TEXT NULL, user_type INTEGER NOT NULL DEFAULT 1, " +
            "created_at DATETIME NULL, updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP)";

    private String create_users_email_unique_index = "CREATE UNIQUE INDEX  email_UNIQUE ON users (email ASC)";

    private String create_table_restaurants = "CREATE TABLE restaurants (_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, master_id INTEGER, " +
            "user_id INTEGER NOT NULL, name TEXT, phone_number TEXT, capacity INTEGER NOT NULL, address TEXT, description TEXT,  latitude REAL NOT NULL, " +
            "longitude REAL NOT NULL, is_deleted INTEGER DEFAULT 0, created_at DATETIME NULL, updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP, " +
            "CONSTRAINT fk_restaurants_users FOREIGN KEY(user_id) REFERENCES users(_id) ON DELETE NO ACTION ON UPDATE NO ACTION)";

    private String create_table_images = "CREATE TABLE images (_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
            "restaurant_id INTEGER NOT NULL, width INTEGER NULL, height INTEGER NULL, image_url TEXT NULL, " +
            "created_at DATETIME NULL, updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP, " +
            "CONSTRAINT fk_images_restaurants FOREIGN KEY(restaurant_id) REFERENCES restaurants(_id) ON DELETE NO ACTION ON UPDATE NO ACTION)";

    private String create_table_reservations = "CREATE TABLE reservations (_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
            "user_id INTEGER NOT NULL, restaurant_id INTEGER NOT NULL, capacity INTEGER NOT NULL, status INTEGER NOT NULL, " +
            "note TEXT NULL, reservation_at DATETIME NULL, is_deleted INTEGER DEFAULT 0, created_at DATETIME NULL, updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP, " +
            "CONSTRAINT fk_reservations_restaurants FOREIGN KEY(restaurant_id) REFERENCES restaurants(_id) ON DELETE NO ACTION ON UPDATE NO ACTION, " +
            "CONSTRAINT fk_reservations_users FOREIGN KEY(user_id) REFERENCES users(_id) ON DELETE NO ACTION ON UPDATE NO ACTION)";

    private String create_table_app_config = "CREATE TABLE app_config (_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
            "db_schema_version INTEGER NULL, static_image_cache TEXT NULL, temp_image_cache TEXT NULL, " +
            "current_user_id INTEGER NULL, last_logged_in_user_id INTEGER NULL, created_at DATETIME NULL, updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP, " +
            "CONSTRAINT fk_app_config_currentuser  FOREIGN KEY (current_user_id) REFERENCES users (_id)  ON DELETE NO ACTION  ON UPDATE NO ACTION, " +
            "CONSTRAINT fk_app_config_lastuser FOREIGN KEY (last_logged_in_user_id) REFERENCES users (_id) ON DELETE NO ACTION  ON UPDATE NO ACTION)";

    private String create_fk_app_config_currentuser_idx_index = "CREATE INDEX fk_app_config_currentuser_idx ON app_config (current_user_id ASC)";
    private String create_fk_app_config_lastuser_idx_index = "CREATE INDEX fk_app_config_lastuser_idx ON app_config (last_logged_in_user_id ASC)";

    private String create_table_reviews = "CREATE TABLE reviews (_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, user_id INTEGER NOT NULL, " +
            "restaurant_id INTEGER NOT NULL, rating REAL NOT NULL, review_text TEXT NULL, created_at DATETIME NULL, updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP, " +
            "CONSTRAINT fk_reviews_restaurants FOREIGN KEY(restaurant_id) REFERENCES restaurants(_id) ON DELETE NO ACTION ON UPDATE NO ACTION, " +
            "CONSTRAINT fk_reviews_users FOREIGN KEY(user_id) REFERENCES users(_id) ON DELETE NO ACTION ON UPDATE NO ACTION)";

    protected boolean createDatabase(SQLiteDatabase db) {
        try {
            db.execSQL(create_users);
            db.execSQL(create_users_email_unique_index);
            db.execSQL(create_table_restaurants);
            db.execSQL(create_table_images);
            db.execSQL(create_table_reservations);
            db.execSQL(create_table_app_config);
            db.execSQL(create_fk_app_config_currentuser_idx_index);
            db.execSQL(create_fk_app_config_lastuser_idx_index);
            db.execSQL(create_table_reviews);

            return true;
        } catch (Exception e) {
            Log.e("", "Error creating the db ", e);
            return false;
        }
    }

    protected boolean insertDummyRestaurantsData(SQLiteDatabase db) {
        try {
            String insertSql[] = {
                    "INSERT INTO users (_id, first_name, last_name, email, password, phone_number, user_type, created_at) VALUES (1,'Dummy1', 'Owner1', 'test1@owner.com', 'password1', '0714253698', 2, datetime('now','localtime'));",
                    "INSERT INTO users (_id, first_name, last_name, email, password, phone_number, user_type, created_at) VALUES (2,'CDummy1', 'Customer1', 'test1@customer.com', '1', '0714253698', 1, datetime('now','localtime'));",
                    "INSERT INTO restaurants (_id, user_id, name, phone_number, capacity, latitude, longitude, created_at) VALUES (1, 1, 'Alaska Restaurant', '0714253698', 233, -1.222373, 36.896602, datetime('now','localtime'));",
                    "INSERT INTO restaurants (_id, user_id, name, phone_number, capacity, latitude, longitude, created_at) VALUES (2, 1, 'Alabama Restaurant', '0714253698', 500, -1.217906, 36.898937, datetime('now','localtime'));",
                    "INSERT INTO restaurants (_id, user_id, name, phone_number, capacity, latitude, longitude, created_at) VALUES (3, 1, 'Florida Restaurant', '0714253698', 740, -1.231090, 36.886924, datetime('now','localtime'));",
                    "INSERT INTO restaurants (_id, user_id, name, phone_number, capacity, latitude, longitude, created_at) VALUES (4, 1, 'Kansas Restaurant', '0714253698', 150, -1.233284, 36.880570, datetime('now','localtime'));",
                    "INSERT INTO restaurants (_id, user_id, name, phone_number, capacity, latitude, longitude, created_at) VALUES (5, 1, 'Indiana Restaurant', '0714253698', 50,-1.233973, 36.900933, datetime('now','localtime'));",
                    "INSERT INTO images (_id, restaurant_id, image_url, created_at) VALUES (1, 1, 'https://dummyimage.com/600x400/000/fff&text=2323', datetime('now','localtime'));",
                    "INSERT INTO reviews(user_id, restaurant_id, review_text, rating, created_at) VALUES (2, 1, 'review for rest1 by user1', 1, datetime());",
                    "INSERT INTO reviews(user_id, restaurant_id, review_text, rating, created_at) VALUES (2, 2, 'review for rest2 by user1', 5, datetime());",
                    "INSERT INTO reviews(user_id, restaurant_id, review_text, rating, created_at) VALUES (2, 3, 'review for rest3 by user1', 4, datetime());",
                    "INSERT INTO reviews(user_id, restaurant_id, review_text, rating, created_at) VALUES (2, 2, 'review for rest2 by user1', 5, datetime());",
                    "INSERT INTO reviews(user_id, restaurant_id, review_text, rating, created_at) VALUES (2, 2, 'review for rest2 by user1', 3, datetime());",
                    "INSERT INTO reviews(user_id, restaurant_id, review_text, rating, created_at) VALUES (2, 2, 'review for rest2 by user1', 0, datetime());"
            };
            Log.i(TAG, "restaurants data populated");
            for (int i = 0; i < insertSql.length; i++) {
                db.execSQL(insertSql[i]);
            }

            return true;
        } catch (Exception e) {
            Log.e(TAG, "Error inserting restaurants data", e);
            return false;
        }
    }
}