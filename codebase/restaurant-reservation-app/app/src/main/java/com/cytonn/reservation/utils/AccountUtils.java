package com.cytonn.reservation.utils;

import android.util.Log;
import android.util.Patterns;

import com.cytonn.reservation.RestaurantReservationApp;
import com.cytonn.reservation.db.dao.DaoSession;
import com.cytonn.reservation.db.models.AppConfig;
import com.cytonn.reservation.db.models.User;

/**
 * Created by F'rdaws on 01-Oct-17.
 */

public class AccountUtils {
    private static final String TAG = AccountUtils.class.getSimpleName();

    private User currentUser;
    DaoSession daoSession;

    {
        daoSession = RestaurantReservationApp.getDaoSessionInstance();
    }

    public static boolean isInvalidEmail(String emailAddress) {
        return emailAddress == null || !emailAddress.matches(Patterns.EMAIL_ADDRESS.pattern());
    }

    public static boolean providedPhoneNumberHasCorrectFormat(String phoneNumber) {

        return !(phoneNumber.length() < 10 || phoneNumber.length() > 11 || !phoneNumber.matches("^[0-9]+$"));

    }

    public User getCurrentUser() {

        if (!hasCurrentUser()) {

            loadCurrentUser();
        }

        return currentUser;
    }

    public boolean hasCurrentUser() {

        return currentUser != null && currentUser.getId() != null;
    }

    public void loadCurrentUser() {
        try {
            if (currentLoggedInUserId() == null) {
                Log.i(TAG, "No user currrently logged in.");
            }
            currentUser = daoSession.getUserDao().load(currentLoggedInUserId());

        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    public void setCurrentUser(User user) {
        AppConfig appConfig;
        if (daoSession.getAppConfigDao().count() > 0) {
            appConfig = daoSession.getAppConfigDao().loadAll().get(0);
        } else {
            appConfig = new AppConfig();
        }

        appConfig.setCurrentUserId(user.getId());
        appConfig.setLastLoggedInUserId(user.getId());

        if (appConfig.getId() == null) {
            daoSession.getAppConfigDao().insertWithoutSettingPk(appConfig);
        } else {
            daoSession.getAppConfigDao().update(appConfig);
        }
        currentUser = user;
    }

    public Long currentLoggedInUserId() {

        if (daoSession.getAppConfigDao().count() > 0) {

            AppConfig appConfig = daoSession.getAppConfigDao().loadAll().get(0);

            if (appConfig != null && appConfig.getCurrentUserId() != null) {

                return appConfig.getCurrentUserId();

            }
        }

        return null;
    }

    public void clearLoggedInUser() {
        if (daoSession.getAppConfigDao().count() > 0) {
            AppConfig appConfig = daoSession.getAppConfigDao().loadAll().get(0);
            appConfig.setLastLoggedInUserId(appConfig.getCurrentUserId());
            appConfig.setCurrentUserId(null);
            daoSession.getAppConfigDao().update(appConfig);
        }
        currentUser = null;
    }
}
