package com.cytonn.reservation.adapters;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cytonn.reservation.R;
import com.cytonn.reservation.db.models.Reservation;
import com.cytonn.reservation.db.models.Review;
import com.cytonn.reservation.shared.Constants;
import com.cytonn.reservation.utils.RestaurantAppUtils;

import java.util.List;

/**
 * Created by F'rdaws on 04-Oct-17.
 */

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.MyViewHolder> {
    public final String TAG = this.getClass().getSimpleName();

    private List<Review> reviewList;
    Context context;

    public List<Review> getReviewList() {
        return reviewList;
    }

    public void setReviewList(List<Review> reviewList) {
        this.reviewList = reviewList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView userName, reviewText, reviewDate;

        public MyViewHolder(View view) {
            super(view);
            userName = (TextView) view.findViewById(R.id.user_name);
            reviewText = (TextView) view.findViewById(R.id.review_text);
            reviewDate = (TextView) view.findViewById(R.id.review_date);
        }
    }

    @Override
    public ReviewsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_review, parent, false);

        return new ReviewsAdapter.MyViewHolder(itemView);
    }

    public ReviewsAdapter(Context context, List<Review> reviewList) {
        this.reviewList = reviewList;
        this.context = context;
    }

    @Override
    public void onBindViewHolder(ReviewsAdapter.MyViewHolder holder, int position) {
        try {
            Review review = reviewList.get(position);
            holder.userName.setText(review.getUser().getFirstName());
            holder.reviewText.setText(review.getReviewText());
            holder.reviewDate.setText(RestaurantAppUtils.getDate(review.getCreatedAt().getTime(), "dd/MM/yyyy hh:mm a"));
        } catch (Exception e){
            Log.e(TAG, e.getMessage(), e);
        }
    }

    @Override
    public int getItemCount() {
        return reviewList.size();
    }
}
