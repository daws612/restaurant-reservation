package com.cytonn.reservation.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.cytonn.reservation.R;
import com.cytonn.reservation.adapters.RestaurantListAdapter;
import com.cytonn.reservation.db.dao.RestaurantDao;
import com.cytonn.reservation.db.models.Restaurant;

import java.util.List;

public class RestaurantListActivity extends BaseActionBarActivity {
    ListView listView;
    RestaurantListAdapter adapter;
    TextView noRests;
    FloatingActionButton addRestFab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_list);

        listView = (ListView) findViewById(R.id.rest_list);

        noRests = (TextView) findViewById(R.id.no_rests);
        addRestFab = (FloatingActionButton) findViewById(R.id.fab_add_rest);

        addRestFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RestaurantListActivity.this, AddRestaurantActivity.class));
            }
        });

        List<Restaurant> restaurantList = daoSession.getRestaurantDao().queryBuilder().where(RestaurantDao.Properties.UserId.eq(accountUtils.getCurrentUser().getId())).list();

        if(restaurantList == null || restaurantList.isEmpty()) {
            noRests.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        }

        RestaurantListAdapter adapter = new RestaurantListAdapter(this, restaurantList);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Restaurant restaurant = (Restaurant) listView.getItemAtPosition(i);
                Intent intent = new Intent(RestaurantListActivity.this, RestaurantDetailsActivity.class);
                intent.putExtra(RestaurantDetailsActivity.RESTAURANT_ID, restaurant.getId());
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mNavigationView.setCheckedItem(R.id.nav_restaurants);
        if(adapter != null)
            adapter.notifyDataSetChanged();
    }
}
