package com.cytonn.reservation.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.cytonn.reservation.R;
import com.cytonn.reservation.RestaurantReservationApp;
import com.cytonn.reservation.shared.Constants;
import com.cytonn.reservation.utils.RestaurantAppUtils;

public class LandingActivity extends BaseActivity {
    
    Button exploreBtn, loginUserBtn, loginOwnerBtn;
    public static String REDIRECT_PAGE = "redirectPage";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        
        exploreBtn = (Button) findViewById(R.id.explore);
        loginUserBtn = (Button) findViewById(R.id.loginUser);
        loginOwnerBtn = (Button) findViewById(R.id.loginOwner);

        if(RestaurantReservationApp.getInstance().getAccountUtils().getCurrentUser() != null && RestaurantReservationApp.getInstance().getAccountUtils().getCurrentUser().getUserType() == Constants.UserType.CUSTOMER) {
            loginOwnerBtn.setVisibility(View.GONE);
            loginUserBtn.setText(getString(R.string.logout_user));
        } else if(RestaurantReservationApp.getInstance().getAccountUtils().getCurrentUser() != null && RestaurantReservationApp.getInstance().getAccountUtils().getCurrentUser().getUserType() == Constants.UserType.OWNER) {
            loginUserBtn.setVisibility(View.GONE);
            loginOwnerBtn.setText(getString(R.string.logout_owner));
        }

        setClickListeners();

        if(getIntent().getBooleanExtra(REDIRECT_PAGE, true)) { //do not redirect if home is clicked on nav drawer
            if (RestaurantAppUtils.isCustomer()) {
                startActivity(new Intent(LandingActivity.this, RestaurantsActivity.class));
                finish();
            } else if (RestaurantAppUtils.isOwner()) {
                startActivity(new Intent(LandingActivity.this, ReservationsActivity.class));
                finish();
            }
        }
    }

    private void setClickListeners() {
        exploreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // open the googleMap with restaurants displayed
                startActivity(new Intent(LandingActivity.this, RestaurantsActivity.class));
            }
        });

        loginOwnerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(loginOwnerBtn.getText().equals(getString(R.string.logout_owner))) {
                    RestaurantReservationApp.getInstance().getAccountUtils().clearLoggedInUser();
                    loginOwnerBtn.setText(getString(R.string.login_owner));
                    loginUserBtn.setVisibility(View.VISIBLE);
                } else {
                    // open loginActivity  to login user of type owner
                    Intent i = new Intent(LandingActivity.this, LoginActivity.class);
                    i.putExtra(LoginActivity.USER_TYPE, Constants.UserType.OWNER);
                    startActivity(i);
                }
            }
        });

        loginUserBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(loginUserBtn.getText().equals(getString(R.string.logout_user))) {
                    RestaurantReservationApp.getInstance().getAccountUtils().clearLoggedInUser();
                    loginUserBtn.setText(getString(R.string.login_user));
                    loginOwnerBtn.setVisibility(View.VISIBLE);
                } else {
                    // open loginActivity  to login user of type customer
                    Intent i = new Intent(LandingActivity.this, LoginActivity.class);
                    i.putExtra(LoginActivity.USER_TYPE, Constants.UserType.CUSTOMER);
                    startActivity(i);
                }
            }
        });
    }
}
