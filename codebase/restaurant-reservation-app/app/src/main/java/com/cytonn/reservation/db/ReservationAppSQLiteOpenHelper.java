package com.cytonn.reservation.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.cytonn.reservation.RestaurantReservationApp;
import com.cytonn.reservation.db.dao.DaoMaster;
import com.cytonn.reservation.db.dao.DaoSession;
import com.cytonn.reservation.shared.Constants;

import org.greenrobot.greendao.identityscope.IdentityScopeType;


public class ReservationAppSQLiteOpenHelper extends InitScripts {

    int version;
    static DaoSession daoSession;
    static DaoMaster daoMasterInstance;

    public ReservationAppSQLiteOpenHelper(Context context, String name, int version) {
        super(context, name, version);
        this.version = version;
    }

    public static DaoSession getDaoSession() {

        if (daoMasterInstance == null) {
            SQLiteDatabase db = ReservationAppSQLiteOpenHelper.getInstance(RestaurantReservationApp.getInstance().getApplicationContext()).getWritableDatabase();
            daoMasterInstance = new DaoMaster(db);
        }

        if (daoSession == null) {
            daoSession = daoMasterInstance.newSession(IdentityScopeType.None);
        }

        return daoSession;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i("", "onCreate called");
        try {
            boolean success = false;
            db.beginTransaction();
            for (int v = 1; v <= version; v++) {
                Log.i("", "Upgrading database to version " + v);
                success = executeUpgradesFor(db, v);
                // If an upgrade fails, abort the process.
                if (!success) {
                    break;
                }
            }
            if (success) {
                db.setTransactionSuccessful();
            }

        } catch (Exception e) {
            Log.e("", "Exception occurred : " + e.getMessage());
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            Log.i("", "Upgrading from version " + oldVersion
                    + " to " + newVersion + ".");
            boolean success = false;

            db.beginTransaction();
            if (newVersion > oldVersion) {
                // perform database upgrades
                for (int v = oldVersion + 1; v <= newVersion; v++) {
                    Log.i("", "Upgrading database to version "
                            + v);
                    success = executeUpgradesFor(db, v);

                    // If an upgrade fails, abort the process.
                    if (!success) {
                        break;
                    }
                }
            }

            if (success) {
                db.setTransactionSuccessful();
            }

        } catch (Exception e) {
            Log.e("", "Exception occurred : " + e.getMessage());
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
    }

    public boolean executeUpgradesFor(SQLiteDatabase db, int version) {

        try {
            boolean success = false;

            switch (version) {
                case 1:
                    success = createDatabase(db);
                    if (success)
                        success = insertDummyRestaurantsData(db);
                    break;
                default:
                    Log.e("", "No upgrades exist for the version number" + version);
                    break;
            }
            return success;

        } catch (Exception e) {
            Log.e("", "Exception occurred during database init : " + e.getMessage(), e);
            return false;
        }
    }

    public static ReservationAppSQLiteOpenHelper getInstance(Context context) {

        if (instance == null) {
            instance = new ReservationAppSQLiteOpenHelper(context, Constants.DATABASE_NAME, Constants.DATABASE_VERSION);
        }

        return instance;
    }

    private static ReservationAppSQLiteOpenHelper instance;
}
