package com.cytonn.reservation.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.cytonn.reservation.R;
import com.cytonn.reservation.adapters.ReservationsAdapter;
import com.cytonn.reservation.db.dao.ReservationDao;
import com.cytonn.reservation.db.dao.RestaurantDao;
import com.cytonn.reservation.db.models.Reservation;
import com.cytonn.reservation.db.models.Restaurant;
import com.cytonn.reservation.notifications.GenericNotification;
import com.cytonn.reservation.shared.Constants;
import com.cytonn.reservation.utils.RestaurantAppUtils;

import org.greenrobot.greendao.query.Query;
import org.greenrobot.greendao.query.QueryBuilder;
import org.greenrobot.greendao.query.WhereCondition;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReservationsActivity extends BaseActionBarActivity implements ReservationsAdapter.EventListener {
    public final String TAG = this.getClass().getSimpleName();

    RecyclerView reservationsRecyclerView;
    ReservationsAdapter reservationsAdapter;
    List<Reservation> reservationList = new ArrayList<>();
    TextView noResvs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservations);
        setTitle(getString(R.string.reservations));

        reservationsRecyclerView = (RecyclerView) findViewById(R.id.reservations_recyclerview);
        noResvs = (TextView) findViewById(R.id.no_resvs);

        reservationsAdapter = new ReservationsAdapter(this, reservationList);
        reservationsAdapter.setmEventListener(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        reservationsRecyclerView.setLayoutManager(mLayoutManager);
        reservationsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        reservationsRecyclerView.setAdapter(reservationsAdapter);

        retrieveReservationsForUser(RestaurantAppUtils.getCurrentUserId());

        if (reservationList == null || reservationList.isEmpty())
            noResvs.setVisibility(View.VISIBLE);

    }

    private void retrieveReservationsForUser(long currentUserId) {
        try {
            QueryBuilder queryBuilder = daoSession.getReservationDao().queryBuilder();
            String isDeleted = "(is_deleted is null or is_deleted=0)";
            if (RestaurantAppUtils.isCustomer())
                reservationList = queryBuilder.where(ReservationDao.Properties.UserId.eq(currentUserId), new WhereCondition.StringCondition(isDeleted)).orderDesc(ReservationDao.Properties.CreatedAt).list();
            else if (RestaurantAppUtils.isOwner()) {
                String restIds = "restaurant_id in (select _id from restaurants where user_id=" + currentUserId + ")";
                isDeleted = "(is_deleted is null or is_deleted=0) and status is not 4";
                reservationList = daoSession.getReservationDao().queryBuilder().where(new WhereCondition.StringCondition(restIds), new WhereCondition.StringCondition(isDeleted)).orderDesc(ReservationDao.Properties.CreatedAt).list();
            }

            reservationsAdapter.setReservationList(reservationList);
            reservationsAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mNavigationView.setCheckedItem(R.id.nav_reservations);
    }

    @Override
    public void onItemCancel(View v) {
        int position = reservationsRecyclerView.getChildAdapterPosition(v);
        Reservation reservation = reservationsAdapter.getItem(position);
        if(reservation.getStatus() == Constants.ReservationStatus.COMPLETED)
            deleteReservation(reservation);
        else
            updateReservationStatus(reservation, Constants.ReservationStatus.CANCELED);
        reservationList.remove(position);
        reservationsAdapter.notifyItemRemoved(position);

        new GenericNotification(this).show("Reservation has been canceled");
    }

    private void deleteReservation(Reservation reservation) {
        if(reservation != null) {
            reservation.setIsDeleted(1);
            reservation.setUpdatedAt(new Date());
            daoSession.getReservationDao().update(reservation);
        }
    }

    @Override
    public void onItemEdit(View v) {
        final int position = reservationsRecyclerView.getChildAdapterPosition(v);
        final Reservation reservation = reservationsAdapter.getItem(position);
        if (RestaurantAppUtils.isCustomer() && reservation != null) {
            Dialog dialog = null;
            if (reservation.getStatus() == Constants.ReservationStatus.COMPLETED) {
                dialog = showReviewDialog(reservation);
            } else
                dialog = showReservationDialog(reservation.getRestaurantId(), reservation);

            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    Reservation reservation = reservationsAdapter.getItem(position);
                    reservation = daoSession.getReservationDao().load(reservation.getId());
                    reservationsAdapter.getReservationList().set(position, reservation);
                    reservationsAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public void onItemConfirm(View v) {
        int position = reservationsRecyclerView.getChildAdapterPosition(v);
        Reservation reservation = reservationsAdapter.getItem(position);
        updateReservationStatus(reservation, Constants.ReservationStatus.CONFIRMED);
        reservationsAdapter.notifyDataSetChanged();

        new GenericNotification(this).show("Your reservation has been confirmed");
    }

    @Override
    public void onItemReject(View v) {
        int position = reservationsRecyclerView.getChildAdapterPosition(v);
        Reservation reservation = reservationsAdapter.getItem(position);
        updateReservationStatus(reservation, Constants.ReservationStatus.REJECTED);
        reservationsAdapter.notifyDataSetChanged();

        new GenericNotification(this).show("Your reservation has been rejected");
    }

    @Override
    public void onInfoItemClick(View v) {
        int position = reservationsRecyclerView.getChildAdapterPosition(v);
        Reservation reservation = reservationsAdapter.getItem(position);
        showAlertDialog(ReservationsActivity.this, getString(R.string.note), reservation.getNote(), null);
    }

    private void updateReservationStatus(Reservation reservation, int status) {
        if (reservation != null) {
            if (status == Constants.ReservationStatus.CANCELED)
                reservation.setIsDeleted(1);
            reservation.setStatus(status);
            reservation.setUpdatedAt(new Date());
            daoSession.getReservationDao().update(reservation);
        }
    }
}
