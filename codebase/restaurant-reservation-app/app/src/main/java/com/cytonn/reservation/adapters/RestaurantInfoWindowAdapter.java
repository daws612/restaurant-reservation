package com.cytonn.reservation.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cytonn.reservation.models.RestaurantItem;
import com.cytonn.reservation.R;
import com.cytonn.reservation.utils.RestaurantRenderer;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

/**
 * Created by Firdaws on 16-Jan-17.
 */
public class RestaurantInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private static final String TAG = RestaurantInfoWindowAdapter.class.getSimpleName();
    private ImageView mImageView;
    private TextView mName;
    private TextView mDistance;
    Context context;
    View view;
    RestaurantRenderer restaurantRenderer;

    public RestaurantInfoWindowAdapter(Context context, RestaurantRenderer restaurantRenderer) {
        this.context = context;
        this.restaurantRenderer = restaurantRenderer;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(final Marker marker) {
        try {
            RestaurantItem restaurantItem = restaurantRenderer.getClusterItem(marker);

            //check if its in a cluster
            if (restaurantItem == null) {
                return null;
            }

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.restaurant_info, null);
            mImageView = (ImageView) view.findViewById(R.id.image);
            mName = (TextView) view.findViewById(R.id.rest_text);

            String name = restaurantItem.getName() != null ? restaurantItem.getName() : "";
            if (name != null) {
                mName.setText(name);
            } else {
                mName.setText(context.getString(R.string.restaurant));
            }


            /*if (restaurantItem != null && !marker.isInfoWindowShown()) {
                String url = null;
                if (restaurantItem.getImageURL() != null && !restaurantItem.getImageURL().isEmpty())
                    url = restaurantItem.getImageURL();

                if (url != null && !url.isEmpty()) {

                    Glide.with(context)
                            .load(url)
                            .placeholder(R.drawable.progress_animation)
                            .error(R.drawable.book_placeholder)
                            .fitCenter()
                            .crossFade()
                            .listener(new RequestListener<MerchandiseImageResource, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, MerchandiseImageResource model, Target<GlideDrawable> target, boolean isFirstResource) {
                                    if (e != null)
                                        LogUtils.l().LOGI(TAG, "Failed to load info popup image: " + e.getMessage(), e);
                                    else
                                        LogUtils.l().LOGI(TAG, "Failed to load info popup image ");

                                    if (mImageView != null) {
                                        mImageView.setImageResource(R.drawable.book_placeholder);
                                    }

                                    if (marker != null && marker.isInfoWindowShown()) {

                                        LogUtils.l().LOGI(TAG, "Reload info popup");
                                        marker.hideInfoWindow();
                                        marker.showInfoWindow();
                                    }
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, MerchandiseImageResource model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    if (marker != null && marker.isInfoWindowShown()) {
                                        marker.hideInfoWindow();
                                        marker.showInfoWindow();
                                    }
                                    return false;
                                }
                            })
                            .into(mImageView);
                } else {

                   *//* Glide.with(context)
                            .load(R.drawable.book_placeholder)
                            .placeholder(R.drawable.progress_animation)
                            .error(R.drawable.book_placeholder)
                            .fitCenter()
                            .crossFade()
                            .listener(new RequestListener<Integer, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, Integer model, Target<GlideDrawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, Integer model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    if (marker != null && marker.isInfoWindowShown()) {
                                        marker.hideInfoWindow();
                                        marker.showInfoWindow();
                                    }
                                    return false;
                                }
                            })
                            .into(mImageView);*//*
                }
            }*/
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return view;
    }
}
