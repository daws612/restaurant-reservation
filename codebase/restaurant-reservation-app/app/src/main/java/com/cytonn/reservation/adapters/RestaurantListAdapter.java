package com.cytonn.reservation.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cytonn.reservation.R;
import com.cytonn.reservation.db.models.Restaurant;

import java.util.List;

/**
 * Created by F'rdaws on 03-Oct-17.
 */

public class RestaurantListAdapter extends ArrayAdapter<Restaurant> {

    private Context context;
    private List<Restaurant> values;

    public RestaurantListAdapter(Context context, List<Restaurant> values) {
        super(context, R.layout.list_item_restaurant, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.list_item_restaurant, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.label);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.logo);
        textView.setText(values.get(position).getName());

        return rowView;
    }

    public RestaurantListAdapter(Context context, int resource) {
        super(context, resource);
    }

    public RestaurantListAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public RestaurantListAdapter(Context context, int resource, Restaurant[] objects) {
        super(context, resource, objects);
    }

    public RestaurantListAdapter(Context context, int resource, int textViewResourceId, Restaurant[] objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public RestaurantListAdapter(Context context, int resource, List<Restaurant> objects) {
        super(context, resource, objects);
    }

    public RestaurantListAdapter(Context context, int resource, int textViewResourceId, List<Restaurant> objects) {
        super(context, resource, textViewResourceId, objects);
    }
}
