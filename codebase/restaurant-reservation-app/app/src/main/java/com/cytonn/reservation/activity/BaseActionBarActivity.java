package com.cytonn.reservation.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cytonn.reservation.R;
import com.cytonn.reservation.RestaurantReservationApp;
import com.cytonn.reservation.db.dao.DaoSession;
import com.cytonn.reservation.db.models.User;
import com.cytonn.reservation.shared.Constants;
import com.cytonn.reservation.utils.AccountUtils;
import com.cytonn.reservation.utils.RestaurantAppUtils;

/**
 * Created by Firdaws on 02-Oct-17.
 */
public class BaseActionBarActivity extends BaseActivity {

    public final String TAG = this.getClass().getSimpleName();

    protected DrawerLayout mDrawerLayout;
    protected NavigationView mNavigationView;
    protected Toolbar toolbar;
    ActionBar actionBar;

    protected DrawerLayout fullLayout;
    protected FrameLayout actContent;

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void setToolbar(Toolbar toolbar) {
        this.toolbar = toolbar;
    }

    @Override
    public void setContentView(final int layoutResID) {
        fullLayout = (DrawerLayout) getLayoutInflater().inflate(R.layout.base_drawer_layout, null); // Your base layout here
        actContent = (FrameLayout) fullLayout.findViewById(R.id.content_frame);
        getLayoutInflater().inflate(layoutResID, actContent, true); // Setting the content of layout your provided to the act_content frame

        super.setContentView(fullLayout);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setContentViewWithNavDrawer();
        if (getSupportActionBar() != null)
            toolbar.setTitle(this.getTitle());
    }

    @Override
    public void onBackPressed() {
        if (fullLayout.isDrawerOpen(GravityCompat.START)) {
            fullLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void setContentViewWithNavDrawer() {

        toolbar = (Toolbar) findViewById(R.id.baseActionBarToolbar);

        if (this instanceof RestaurantDetailsActivity) {
            toolbar.setVisibility(View.GONE);
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
        } else {
            setSupportActionBar(toolbar);
        }
        actionBar = getSupportActionBar();

        setupNavigationDrawer();
    }

    public void setupNavigationDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationView = (NavigationView) findViewById(R.id.navigation);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                mDrawerLayout.closeDrawers();
                menuItem.setChecked(true);
                selectItem(menuItem.getItemId());
                return true;
            }
        });

        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        View headerView = mNavigationView.getHeaderView(0);
        TextView userName = (TextView) headerView.findViewById(R.id.textViewNameNav);
        TextView email = (TextView) headerView.findViewById(R.id.textViewEmail);
        TextView phone = (TextView) headerView.findViewById(R.id.textViewPhone);

        User user = accountUtils.getCurrentUser();
        if (user != null) {
            userName.setText(user.getFirstName() + " " + user.getLastName());
            email.setText(user.getEmail());
            phone.setText(user.getPhoneNumber());
            mNavigationView.getMenu().findItem(R.id.nav_reservations).setVisible(true);

        } else {
            mNavigationView.getMenu().findItem(R.id.nav_reservations).setVisible(false);
            mNavigationView.getMenu().findItem(R.id.nav_restaurants).setVisible(false);
        }
    }

    public void selectItem(int id) {
        switch (id) {
            case R.id.nav_home:
                Intent homeIntent = new Intent(BaseActionBarActivity.this, LandingActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                homeIntent.putExtra(LandingActivity.REDIRECT_PAGE, false);
                startActivity(homeIntent);
                finish();
                break;
            case R.id.nav_reservations:
                Intent resvIntent = new Intent(BaseActionBarActivity.this, ReservationsActivity.class);
                resvIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(resvIntent);
                break;
            case R.id.nav_restaurants:
                if (accountUtils.getCurrentUser().getUserType() == Constants.UserType.CUSTOMER) {
                    Intent restIntent = new Intent(BaseActionBarActivity.this, RestaurantsActivity.class);
                    restIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(restIntent);
                } else if(RestaurantAppUtils.isOwner()) {
                    Intent restIntent = new Intent(BaseActionBarActivity.this, RestaurantListActivity.class);
                    restIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    startActivity(restIntent);
                }
                break;
            case R.id.nav_help:
                Toast.makeText(RestaurantReservationApp.getContext(), "Opens help", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
