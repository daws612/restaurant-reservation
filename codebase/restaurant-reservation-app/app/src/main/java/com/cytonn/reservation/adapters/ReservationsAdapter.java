package com.cytonn.reservation.adapters;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cytonn.reservation.R;
import com.cytonn.reservation.RestaurantReservationApp;
import com.cytonn.reservation.db.dao.DaoSession;
import com.cytonn.reservation.db.models.Reservation;
import com.cytonn.reservation.shared.Constants;
import com.cytonn.reservation.utils.RestaurantAppUtils;
import com.h6ah4i.android.widget.advrecyclerview.utils.RecyclerViewAdapterUtils;

import java.util.Date;
import java.util.List;

/**
 * Created by F'rdaws on 02-Oct-17.
 */

public class ReservationsAdapter extends RecyclerView.Adapter<ReservationsAdapter.MyViewHolder>  {
    public final String TAG = this.getClass().getSimpleName();

    private List<Reservation> reservationList;
    private View.OnClickListener mEditButtonOnClickListener;
    private View.OnClickListener mCancelButtonOnClickListener;
    private View.OnClickListener mConfirmButtonOnClickListener;
    private View.OnClickListener mRejectButtonOnClickListener;
    private View.OnClickListener mInfoButtonOnClickListener;
    private EventListener mEventListener;

    Context context;

    public interface EventListener {

        void onItemCancel(View v);

        void onItemEdit(View v);

        void onItemConfirm(View v);

        void onItemReject(View v);

        void onInfoItemClick(View v);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView restName, userDetails, resvDate, resvCapacity, resvStatus;
        public FloatingActionButton cancelBtn, confirmBtn, editBtn, rejectBtn;
        public ImageView info;

        public MyViewHolder(View view) {
            super(view);
            restName = (TextView) view.findViewById(R.id.rest_name);
            userDetails = (TextView) view.findViewById(R.id.customer_details);
            resvDate = (TextView) view.findViewById(R.id.reservation_datetime);
            resvCapacity = (TextView) view.findViewById(R.id.reservation_capacity);
            resvStatus = (TextView) view.findViewById(R.id.reservation_status);

            cancelBtn = (FloatingActionButton) view.findViewById(R.id.cancel_fab_button);
            confirmBtn = (FloatingActionButton) view.findViewById(R.id.confirm_fab_button);
            editBtn = (FloatingActionButton) view.findViewById(R.id.edit_fab_button);
            rejectBtn = (FloatingActionButton) view.findViewById(R.id.reject_fab_button);
            info = (ImageView) view.findViewById(R.id.note);
        }
    }

    public ReservationsAdapter(Context context, List<Reservation> reservationList) {
        this.reservationList = reservationList;
        this.context = context;

        mEditButtonOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEventListener != null) {
                    mEventListener.onItemEdit(RecyclerViewAdapterUtils.getParentViewHolderItemView(v));
                }
            }
        };

        mCancelButtonOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEventListener != null) {
                    mEventListener.onItemCancel(RecyclerViewAdapterUtils.getParentViewHolderItemView(v));
                }
            }
        };

        mConfirmButtonOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEventListener != null) {
                    mEventListener.onItemConfirm(RecyclerViewAdapterUtils.getParentViewHolderItemView(v));
                }
            }
        };

        mRejectButtonOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEventListener != null) {
                    mEventListener.onItemReject(RecyclerViewAdapterUtils.getParentViewHolderItemView(v));
                }
            }
        };

        mInfoButtonOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mEventListener != null) {
                    mEventListener.onInfoItemClick(RecyclerViewAdapterUtils.getParentViewHolderItemView(v));
                }
            }
        };
    }

    public void setReservationList(List<Reservation> reservationList) {
        this.reservationList = reservationList;
    }

    public List<Reservation> getReservationList() {
        return reservationList;
    }

    public void setmEventListener(EventListener mEventListener) {
        this.mEventListener = mEventListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_reservation, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        try {
            Reservation reservation = reservationList.get(position);
            holder.restName.setText(reservation.getRestaurant().getName());
            holder.userDetails.setText(reservation.getUser().getFirstName());
            holder.resvDate.setText(RestaurantAppUtils.getDate(reservation.getReservationAt(), "dd/MM/yyyy hh:mm a"));
            holder.resvCapacity.setText(context.getString(R.string.resv_capacity, reservation.getCapacity()));

            if(System.currentTimeMillis() - reservation.getReservationAt() > 1000) {
                //set reservation status to completed if reservation date has passed and allow user to add a review
                reservation.setStatus(Constants.ReservationStatus.COMPLETED);
                reservation.setUpdatedAt(new Date());
                RestaurantReservationApp.getDaoSessionInstance().getReservationDao().update(reservation);
            }

            holder.resvStatus.setText(Constants.ReservationStatus.getReadableStatus(reservation.getStatus()));
            if(reservation.getNote() != null && !reservation.getNote().trim().isEmpty())
                holder.info.setVisibility(View.VISIBLE);

            if(RestaurantAppUtils.isCustomer()) {
                holder.userDetails.setVisibility(View.GONE);
                holder.confirmBtn.setVisibility(View.GONE);
                holder.rejectBtn.setVisibility(View.GONE);
            } else if(RestaurantAppUtils.isOwner()) {
                holder.userDetails.setVisibility(View.VISIBLE);
                holder.cancelBtn.setVisibility(View.GONE);
                holder.editBtn.setVisibility(View.GONE);
                if(reservation.getStatus() == Constants.ReservationStatus.CONFIRMED)
                    holder.confirmBtn.setVisibility(View.INVISIBLE);
                if(reservation.getStatus() == Constants.ReservationStatus.REJECTED) {
                    holder.confirmBtn.setVisibility(View.INVISIBLE);
                }
            }

            if(reservation.getStatus() == Constants.ReservationStatus.COMPLETED) {
                holder.editBtn.setImageResource(R.drawable.add); //change it to be able to add review now
                holder.confirmBtn.setVisibility(View.GONE);
                holder.rejectBtn.setVisibility(View.GONE);
            }

            holder.editBtn.setOnClickListener(mEditButtonOnClickListener);
            holder.cancelBtn.setOnClickListener(mCancelButtonOnClickListener);
            holder.confirmBtn.setOnClickListener(mConfirmButtonOnClickListener);
            holder.rejectBtn.setOnClickListener(mRejectButtonOnClickListener);
            holder.info.setOnClickListener(mInfoButtonOnClickListener);

        } catch (Exception e){
            Log.e(TAG, e.getMessage(), e);
        }
    }

    @Override
    public int getItemCount() {
        return reservationList.size();
    }

    public Reservation getItem(int position) {
        return reservationList.get(position);
    }
}
