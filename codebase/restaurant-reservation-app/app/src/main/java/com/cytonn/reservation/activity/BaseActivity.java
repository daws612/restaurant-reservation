package com.cytonn.reservation.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.TimePicker;

import com.cytonn.reservation.R;
import com.cytonn.reservation.RestaurantReservationApp;
import com.cytonn.reservation.callbacks.DialogAction;
import com.cytonn.reservation.db.dao.DaoSession;
import com.cytonn.reservation.db.models.Reservation;
import com.cytonn.reservation.db.models.Review;
import com.cytonn.reservation.shared.Constants;
import com.cytonn.reservation.utils.AccountUtils;
import com.cytonn.reservation.utils.RestaurantAppUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by F'rdaws on 30-Sep-17.
 */

public class BaseActivity extends AppCompatActivity implements View.OnClickListener {
    private final String TAG = this.getClass().getSimpleName();
    public final static String INTENDED_ACTIVITY = "intended_activity";

    DaoSession daoSession;
    AccountUtils accountUtils = RestaurantReservationApp.getInstance().getAccountUtils();

    private Dialog mProgressDialog;
    Dialog loginDialog;

    Button date, time;
    EditText capacity, note;
    TextView error, reviewText;
    RatingBar rating;
    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        daoSession = RestaurantReservationApp.getDaoSessionInstance();
    }

    protected void createProgressDialog() {
        mProgressDialog = new Dialog(BaseActivity.this, R.style.progress_dialog_theme);
        mProgressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mProgressDialog.setContentView(R.layout.progress_dialog);
        mProgressDialog.setCancelable(true);
    }

    public void showProgressPopup() {
        if (mProgressDialog == null)
            createProgressDialog();
        mProgressDialog.show();
    }

    public void showProgressPopup(String message) {
        if (mProgressDialog == null)
            createProgressDialog();
        if (mProgressDialog != null) {
            TextView messageText = (TextView) mProgressDialog.findViewById(R.id.textViewMessage);
            messageText.setText(message);
            showProgressPopup();
        }
    }

    public void hideProgressPopup() {
        if (mProgressDialog != null) {
            TextView messageText = (TextView) mProgressDialog.findViewById(R.id.textViewMessage);
            messageText.setText("");
            mProgressDialog.dismiss();
        }
    }


    public Dialog showAlertDialog(Context context, String title, String message, final DialogAction action) {
        if (context != null) {
            try {
                return new AlertDialog.Builder(context).setTitle(title).setMessage(message)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                if (action != null)
                                    action.getAction();
                            }
                        }).show();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }
        return null;
    }

    public void showLoginRequiredDialog() {
        if (RestaurantReservationApp.getInstance().getAccountUtils().getCurrentUser() != null
                && RestaurantReservationApp.getInstance().getAccountUtils().getCurrentUser().getUserType() == Constants.UserType.OWNER)
            showAlertDialog(BaseActivity.this, getString(R.string.reserve), getString(R.string.login_required_owner), new DialogAction() {
                @Override
                public void getAction() {
                    Intent i = new Intent(BaseActivity.this, LoginActivity.class);
                    i.putExtra(INTENDED_ACTIVITY, LandingActivity.class.getSimpleName());
                    startActivity(i);
                }
            });
        else
            showAlertDialog(BaseActivity.this, getString(R.string.reserve), getString(R.string.login_required), new DialogAction() {
                @Override
                public void getAction() {
                    Intent i = new Intent(BaseActivity.this, LoginActivity.class);
                    i.putExtra(LoginActivity.USER_TYPE, Constants.UserType.CUSTOMER);
                    i.putExtra(INTENDED_ACTIVITY, RestaurantDetailsActivity.class.getSimpleName());
                    startActivity(i);
                }
            });
    }

    protected Dialog showReservationDialog(final long restaurantId, final Reservation reservation) {
        final LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.reservation_dialog, null);
        AlertDialog.Builder reserveDialogBuilder = new AlertDialog.Builder(BaseActivity.this).setTitle(getString(R.string.reserve))
                .setView(dialogView)
                .setPositiveButton(getString(R.string.reserve), null)
                .setNegativeButton(getString(android.R.string.cancel), null);

        Dialog dialog = reserveDialogBuilder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(final DialogInterface dialog) {

                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        error.setVisibility(View.GONE);

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(mYear, mMonth, mDay, mHour, mMinute, 0);
                        long reserveTime = calendar.getTimeInMillis();

                        if (capacity.getText() == null || capacity.getText().toString().isEmpty() ||
                                date.getText() == null || date.getText().toString().isEmpty() || !date.getText().toString().contains("/") ||
                                time.getText() == null || time.getText().toString().isEmpty() || !time.getText().toString().contains(":")) {
                            error.setVisibility(View.VISIBLE);
                        } else if (System.currentTimeMillis() - reserveTime > 1000) {
                            error.setVisibility(View.VISIBLE);
                            error.setText("Please select a future date and time");
                        } else {
                            //add to db then show it has been reserved
                            boolean success = addNewReservation(reservation != null ? reservation.getId() : null, restaurantId, Integer.valueOf(capacity.getText().toString()), reserveTime, note.getText().toString());
                            dialog.dismiss();
                            if (success)
                                showAlertDialog(BaseActivity.this, getString(R.string.made_reservation), getString(R.string.made_reservation_details, capacity.getText().toString(), date.getText().toString(), time.getText().toString()), null);
                            else
                                showAlertDialog(BaseActivity.this, getString(R.string.made_reservation), getString(R.string.made_reservation_error), null);
                        }
                    }
                });
            }
        });

        date = (Button) dialogView.findViewById(R.id.date);
        time = (Button) dialogView.findViewById(R.id.time);
        capacity = (EditText) dialogView.findViewById(R.id.number);
        error = (TextView) dialogView.findViewById(R.id.error);
        note = (EditText) dialogView.findViewById(R.id.note);

        date.setOnClickListener(this);
        time.setOnClickListener(this);

        final Calendar c = Calendar.getInstance();

        if (reservation != null) {
            //populate fileds for editing
            capacity.setText(reservation.getCapacity().toString());
            note.setText(reservation.getNote());
            date.setText(RestaurantAppUtils.getDate(reservation.getReservationAt(), "dd/MM/yyyy"));
            time.setText(RestaurantAppUtils.getDate(reservation.getReservationAt(), "HH:mm"));
            c.setTimeInMillis(reservation.getReservationAt());
        }
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        dialog.show();

        return dialog;

    }

    @Override
    public void onClick(View v) {

        if (v == date) {

            try {
                // Get Current Date


                DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        date.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                        mYear = year;
                        mMonth = monthOfYear;
                        mDay = dayOfMonth;
                    }
                }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (v == time) {

            try {
                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        String min = String.valueOf(minute);
                        if (minute < 10)
                            min = "0".concat(String.valueOf(minute));
                        time.setText(hourOfDay + ":" + min);
                        mHour = hourOfDay;
                        mMinute = minute;
                    }
                }, mHour, mMinute, true);
                timePickerDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    protected boolean addNewReservation(Long reservationId, long restaurantId, int capacity, long reserveTime, String note) {
        try {
            Reservation reservation = new Reservation();

            if (reservationId != null)
                reservation = daoSession.getReservationDao().load(reservationId);

            reservation.setReservationAt(reserveTime);
            reservation.setCapacity(capacity);
            reservation.setRestaurantId(restaurantId);
            reservation.setStatus(Constants.ReservationStatus.PENDING);
            reservation.setUserId(RestaurantReservationApp.getInstance().getAccountUtils().getCurrentUser().getId());
            reservation.setNote(note);

            if (reservationId != null) {
                reservation.setUpdatedAt(new Date());
                daoSession.getReservationDao().update(reservation);
            } else {
                reservation.setCreatedAt(new Date());
                daoSession.getReservationDao().insert(reservation);
            }
            return true;
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
            e.printStackTrace();
            return false;
        }
    }

    protected void closeSoftKeyboard() {

        try {
            InputMethodManager imm = (InputMethodManager) this.getSystemService(this.INPUT_METHOD_SERVICE);
            //Find the currently focused view, so we can grab the correct window token from it.
            View view = this.getCurrentFocus();
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = new View(this);
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            Log.i(TAG, "Error closing soft keyboard", e);
        }
    }

    protected Dialog showReviewDialog(final Reservation reservation) {

        final LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.review_dialog, null);
        AlertDialog.Builder reserveDialogBuilder = new AlertDialog.Builder(BaseActivity.this).setTitle(getString(R.string.tell_us))
                .setView(dialogView)
                .setPositiveButton(getString(R.string.submit), null)
                .setNegativeButton(getString(android.R.string.cancel), null);

        Dialog dialog = reserveDialogBuilder.create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(final DialogInterface dialog) {

                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        error.setVisibility(View.GONE);

                        Double ratingValue = new Double(rating.getRating());

                        if (reviewText.getText() == null || reviewText.getText().toString().isEmpty() && ratingValue <= 0.0) {
                            error.setVisibility(View.VISIBLE);
                        } else {
                            //add to db then show it has been added
                            boolean success = addNewReview(reservation.getRestaurantId(), ratingValue, reviewText.getText() != null ? reviewText.getText().toString() : null);
                            dialog.dismiss();
                            if (success)
                                showAlertDialog(BaseActivity.this, getString(R.string.made_review), getString(R.string.made_review_success), null);
                            else
                                showAlertDialog(BaseActivity.this, getString(R.string.made_review), getString(R.string.made_reservation_error), null);
                        }
                    }
                });
            }
        });

        reviewText = (TextView) dialogView.findViewById(R.id.review_text);
        rating = (RatingBar) dialogView.findViewById(R.id.rate);
        error = (TextView) dialogView.findViewById(R.id.error);

        dialog.show();

        return dialog;

    }

    protected boolean addNewReview(long restaurantId, double rating, String reviewText) {
        try {
            Review review = new Review();

            review.setRestaurantId(restaurantId);
            review.setUserId(RestaurantReservationApp.getInstance().getAccountUtils().getCurrentUser().getId());
            review.setReviewText(reviewText);
            review.setRating(rating);
            review.setCreatedAt(new Date());
            daoSession.getReviewDao().insert(review);

            return true;
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
            e.printStackTrace();
            return false;
        }
    }

}
