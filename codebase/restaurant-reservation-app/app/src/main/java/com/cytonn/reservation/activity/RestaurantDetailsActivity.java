package com.cytonn.reservation.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cytonn.reservation.R;
import com.cytonn.reservation.RestaurantReservationApp;
import com.cytonn.reservation.adapters.ReservationsAdapter;
import com.cytonn.reservation.adapters.ReviewsAdapter;
import com.cytonn.reservation.db.dao.ReservationDao;
import com.cytonn.reservation.db.dao.RestaurantDao;
import com.cytonn.reservation.db.dao.ReviewDao;
import com.cytonn.reservation.db.models.Restaurant;
import com.cytonn.reservation.db.models.Review;
import com.cytonn.reservation.shared.Constants;
import com.cytonn.reservation.utils.EndlessRecyclerViewScrollListener;
import com.cytonn.reservation.utils.RestaurantAppUtils;

import java.util.ArrayList;
import java.util.List;

public class RestaurantDetailsActivity extends BaseActionBarActivity{
    public static final String TAG = RestaurantsActivity.class.getSimpleName();
    public static final String RESTAURANT_ID = "restaurant_id";

    RelativeLayout noDetailsLayout, detailsLayout;
    RecyclerView reviewsRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    TextView addressView, phoneView, capacityView, descriptionView;
    RatingBar restRatingBar;
    private EndlessRecyclerViewScrollListener scrollListener;
    ReviewsAdapter reviewsAdapter;
    private List<Review> reviewList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        daoSession = RestaurantReservationApp.getDaoSessionInstance();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        if(RestaurantAppUtils.isOwner()) {
            fab.setImageResource(R.drawable.ic_mode_edit_white_24dp);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(RestaurantDetailsActivity.this, AddRestaurantActivity.class);
                    intent.putExtra(AddRestaurantActivity.RESTAURANT_ID, getIntent().getLongExtra(RESTAURANT_ID, -1));
                    startActivity(intent);
                }
            });
        } else {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (RestaurantReservationApp.getInstance().getAccountUtils().getCurrentUser() != null
                            && RestaurantReservationApp.getInstance().getAccountUtils().getCurrentUser().getUserType() == Constants.UserType.CUSTOMER)
                        showReservationDialog(getIntent().getLongExtra(RESTAURANT_ID, -1), null);
                    else
                        showLoginRequiredDialog();
                }
            });
        }


        noDetailsLayout = (RelativeLayout) findViewById(R.id.no_details);
        detailsLayout = (RelativeLayout) findViewById(R.id.rest_details);
        reviewsRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        addressView = (TextView) findViewById(R.id.rest_address);
        phoneView = (TextView) findViewById(R.id.rest_phone);
        capacityView = (TextView) findViewById(R.id.rest_capacity);
        descriptionView = (TextView) findViewById(R.id.rest_desciption);
        restRatingBar = (RatingBar) findViewById(R.id.rest_rating);
        mLayoutManager = new LinearLayoutManager(this);

        //add scroll listener
        scrollListener = new EndlessRecyclerViewScrollListener((LinearLayoutManager) mLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                //fetch more reviews
            }
        };

        reviewsRecyclerView.addOnScrollListener(scrollListener);
        reviewsAdapter = new ReviewsAdapter(this, reviewList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        reviewsRecyclerView.setLayoutManager(mLayoutManager);
        reviewsRecyclerView.setItemAnimator(new DefaultItemAnimator());
        reviewsRecyclerView.setAdapter(reviewsAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(reviewsRecyclerView.getContext(), DividerItemDecoration.VERTICAL);
        reviewsRecyclerView.addItemDecoration(dividerItemDecoration);

        if (getIntent().hasExtra(RESTAURANT_ID)) {
            noDetailsLayout.setVisibility(View.GONE);
            detailsLayout.setVisibility(View.VISIBLE);
            Long id = getIntent().getLongExtra(RESTAURANT_ID, -1);
            Restaurant restaurant = getRestaurantFromId(id);
            if (restaurant != null && restaurant.getCapacity() != null) {
                populateDetails(restaurant);
            } else {
                noDetailsLayout.setVisibility(View.VISIBLE);
                detailsLayout.setVisibility(View.GONE);
            }
        } else {
            noDetailsLayout.setVisibility(View.VISIBLE);
            detailsLayout.setVisibility(View.GONE);
        }
    }

    private void populateDetails(Restaurant restaurant) {
        CollapsingToolbarLayout layout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        layout.setTitle(restaurant.getName());

        addressView.setText(getString(R.string.rest_address, restaurant.getName().concat(restaurant.getAddress() != null ? restaurant.getAddress() : "")));
        phoneView.setText(getString(R.string.rest_phone, restaurant.getPhoneNumber()));
        capacityView.setText(getString(R.string.rest_capacity, restaurant.getCapacity()));
        restRatingBar.setRating(calculateAverage(restaurant.getReviews()));
        if (restaurant.getDescription() != null && !restaurant.getDescription().isEmpty() && !restaurant.getDescription().trim().isEmpty()) {
            descriptionView.setText(restaurant.getDescription().trim());
            descriptionView.setVisibility(View.VISIBLE);
        }

        //populate review texts in recycler view

        reviewList = daoSession.getReviewDao().queryBuilder().where(ReviewDao.Properties.RestaurantId.eq(restaurant.getId()), ReviewDao.Properties.ReviewText.isNotNull(), ReviewDao.Properties.ReviewText.notEq("")).orderDesc(ReviewDao.Properties.CreatedAt).list();
        if(reviewList != null) {
            reviewsAdapter.setReviewList(reviewList);
            reviewsAdapter.notifyDataSetChanged();
        }

    }

    private float calculateAverage(List<Review> reviews) {
        float sum = 0;
        if (reviews != null && !reviews.isEmpty()) {
            for (Review mark : reviews) {
                sum += mark.getRating();
            }
            return sum / reviews.size();
        }
        return sum;
    }

    private Restaurant getRestaurantFromId(Long id) {
        Restaurant restaurant = daoSession.getRestaurantDao().queryBuilder().where(RestaurantDao.Properties.Id.eq(id)).unique();
        return restaurant;

    }
}
