package com.cytonn.reservation.activity;

import android.Manifest;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.Toast;

import com.cytonn.reservation.R;
import com.cytonn.reservation.models.RestaurantItem;
import com.cytonn.reservation.shared.Constants;
import com.cytonn.reservation.tasks.RetrieveRestaurantsTask;
import com.cytonn.reservation.adapters.RestaurantInfoWindowAdapter;
import com.cytonn.reservation.utils.RestaurantRenderer;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;

import java.util.ArrayList;
import java.util.List;

public class RestaurantsActivity extends BaseActionBarActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, ClusterManager.OnClusterClickListener<RestaurantItem> {
    public static final String TAG = RestaurantsActivity.class.getSimpleName();

    private GoogleMap mGoogleMap;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private ClusterManager<RestaurantItem> mClusterManager;
    private RestaurantRenderer mRenderer;
    List<RestaurantItem> mClusterItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurants);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);

        // Retrieve the SearchView and plug it into SearchManager
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Check if no view has focus:
                showProgressPopup("Searching restaurants...");
                RetrieveRestaurantsTask retrieveRestaurantsTask = new RetrieveRestaurantsTask(retrieveRestaurantsTaskCallback);
                retrieveRestaurantsTask.execute(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public void onMapReady(GoogleMap mGoogleMap) {
        this.mGoogleMap = mGoogleMap;

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                //Location Permission already granted
                buildGoogleApiClient();
                this.mGoogleMap.setMyLocationEnabled(true);
                setUpClusterer();
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
            this.mGoogleMap.setMyLocationEnabled(true);
            setUpClusterer();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onLocationChanged(Location location) {
        if (mLastLocation == null) {
            //move map camera on first time only
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), Constants.DEFAULT_ZOOM));
        }
        mLastLocation = location;

    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                new AlertDialog.Builder(this)
                        .setTitle("Location Permission Needed")
                        .setMessage("This app needs the Location permission, please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(RestaurantsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mGoogleMap.setMyLocationEnabled(true);
                        setUpClusterer();
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private void setUpClusterer() {

        if (mGoogleMap == null)
            return;

        // Position the map.
        if (mLastLocation != null) {
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), Constants.DEFAULT_ZOOM));
        }

        // Initialize the manager with the context and the map.
        // (Activity extends context, so we can pass 'this' in the constructor.)
        mClusterManager = new ClusterManager<RestaurantItem>(this, mGoogleMap);
        mRenderer = new RestaurantRenderer(this, mGoogleMap, mClusterManager);
        mClusterManager.setRenderer(mRenderer);
        mClusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<RestaurantItem>() {
            @Override
            public boolean onClusterItemClick(RestaurantItem restaurantItem) {

                if (restaurantItem == null)
                    return false;

                if (!mRenderer.getMarker(restaurantItem).isInfoWindowShown())
                    mRenderer.getMarker(restaurantItem).showInfoWindow();

                return true;
            }
        });

        // Point the map's listeners at the listeners implemented by the cluster manager.
        mGoogleMap.setOnCameraChangeListener(mClusterManager);
        mGoogleMap.setOnMarkerClickListener(mClusterManager);
        mGoogleMap.setOnInfoWindowClickListener(mClusterManager);
        mGoogleMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());
        mRenderer.setOnClusterClickListener(this);
        mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(new RestaurantInfoWindowAdapter(this, mRenderer));
        mClusterManager.getClusterMarkerCollection().setOnInfoWindowAdapter(new RestaurantInfoWindowAdapter(this, mRenderer));

        mClusterManager.getMarkerCollection().setOnInfoWindowClickListener(infoWindowClickListener);
        mClusterManager.getClusterMarkerCollection().setOnInfoWindowClickListener(infoWindowClickListener);

        showProgressPopup("Retrieving restaurants");
        //Retrieve the available restaurants
        RetrieveRestaurantsTask retrieveRestaurantsTask = new RetrieveRestaurantsTask(retrieveRestaurantsTaskCallback);
        retrieveRestaurantsTask.execute();

    }

    private void setupClusterMarkers() {
        try {
            //first clear current items from the map
            if (mClusterManager != null)
                mClusterManager.clearItems();
            else return;

            mClusterManager.addItems(mClusterItems);
            mClusterManager.setOnClusterClickListener(this);
            mClusterManager.cluster();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    @Override
    public boolean onClusterClick(Cluster<RestaurantItem> cluster) {
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (ClusterItem item : cluster.getItems()) {
            builder.include(item.getPosition());
        }

        // Get the LatLngBounds
        final LatLngBounds bounds = builder.build();

        // Animate camera to the bounds
        try {
            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 0));
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
        }
        return false;
    }

    GoogleMap.OnInfoWindowClickListener infoWindowClickListener = new GoogleMap.OnInfoWindowClickListener() {
        @Override
        public void onInfoWindowClick(Marker marker) {
            RestaurantItem item = mRenderer.getClusterItem(marker);
            Intent intent = new Intent(RestaurantsActivity.this, RestaurantDetailsActivity.class);
            intent.putExtra(RestaurantDetailsActivity.RESTAURANT_ID, item.getId());
            startActivity(intent);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        mNavigationView.setCheckedItem(R.id.nav_restaurants);
    }

    RetrieveRestaurantsTask.Callback retrieveRestaurantsTaskCallback = new RetrieveRestaurantsTask.Callback() {
        @Override
        public void finished(final List<RestaurantItem> restaurantItemList) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mClusterItems = restaurantItemList;
                    setupClusterMarkers();
                    hideProgressPopup();

                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mClusterItems.get(0).getPosition(), Constants.DEFAULT_ZOOM));
                }
            });
        }

        @Override
        public void error(final String message) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showAlertDialog(RestaurantsActivity.this, getString(R.string.app_name), message, null);
                    hideProgressPopup();
                }
            });
        }
    };

}
