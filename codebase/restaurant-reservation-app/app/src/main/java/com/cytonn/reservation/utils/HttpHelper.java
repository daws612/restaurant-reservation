package com.cytonn.reservation.utils;

import android.util.Log;


import com.cytonn.reservation.db.models.Restaurant;
import com.cytonn.reservation.shared.Constants;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.ProtocolException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class HttpHelper {
    private final String TAG = HttpHelper.class.getSimpleName();


    public byte[] sendPost(String url, HttpEntity entity) throws IOException {
        Log.i(TAG, "******************");
       Log.i(TAG, "POST to "+url);
        URL obj = new URL(url);
        HttpURLConnection urlConnection = (HttpURLConnection) obj.openConnection();
        urlConnection.setDoInput(true);
        urlConnection.setDoOutput(true);
        urlConnection.setUseCaches(false);
        urlConnection.setRequestMethod("POST");
        urlConnection.setConnectTimeout(Constants.HTTP_CONNECTION_TIMEOUT_SHORT); //15 seconds
        urlConnection.setReadTimeout(Constants.HTTP_CONNECTION_TIMEOUT_LONG);//60 seconds

        urlConnection.setRequestProperty("Connection", "Keep-Alive");
        urlConnection.addRequestProperty("Content-length", entity.getContentLength()+"");
        urlConnection.addRequestProperty(entity.getContentType().getName(), entity.getContentType().getValue());

        DataOutputStream dStream = new DataOutputStream(urlConnection.getOutputStream());
        entity.writeTo(dStream);
        dStream.flush();
        dStream.close();

        byte[] response = readResponse(urlConnection);
        Log.i(TAG, "******************");
        return response;
    }

    public byte[] sendGet(String url) throws IOException {
        return sendGet(url, null);
    }

    public byte[] sendGet(String url, List<NameValuePair> params)throws IOException{
        Log.i(TAG, "******************");
        Log.i(TAG, "GET from "+url);
        String urlString;
        if (params != null) {
            urlString = url.concat(CustomHttpHandler.addParameters(params));
        } else {
            urlString = url;
        }

        URL obj = new URL(urlString);
        HttpURLConnection urlConnection = (HttpURLConnection) obj.openConnection();

        // optional, default is GET
        urlConnection.setRequestMethod("GET");

        byte[] response = readResponse(urlConnection);
        Log.i(TAG, "******************");
        return response;
    }

    private byte[] readResponse(HttpURLConnection urlConnection) throws IOException {
        int statusCode = urlConnection.getResponseCode();

        byte[] responseData;
        if(statusCode == HttpURLConnection.HTTP_OK){
            Log.i(TAG, "HTTP Response Status OK");
            // Read in the response bytes
            InputStream is = urlConnection.getInputStream();
            DataInputStream responseDataInputStream = new DataInputStream(is);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[responseDataInputStream.available()];

            while (responseDataInputStream.read(buffer) != -1) {
                Log.i(TAG,"Reading "+buffer.length+" bytes...");
                byteArrayOutputStream.write(buffer);
                int available = responseDataInputStream.available();
                if (available < 1) {
                    available = 1;
                }
                buffer = new byte[available];
            }

            responseData = byteArrayOutputStream.toByteArray();
        }else{
            throw new IOException("Server returned status: " + statusCode);
        }
        return responseData;
    }
}
