package com.cytonn.reservation.shared;

import com.cytonn.reservation.R;
import com.cytonn.reservation.RestaurantReservationApp;

/**
 * Created by Firdaws on 29-Sep-17.
 */
public class Constants {

    private static String serverUrl = RestaurantReservationApp.getInstance().getString(R.string.server_url);

    public static final class UserType {
        public static final int CUSTOMER = 1;
        public static final int OWNER = 2;
    }

    public static Integer DEFAULT_ZOOM = 16;

    public static final String DATABASE_NAME = "restaurant.reservation.db";
    public static final int DATABASE_VERSION = 1;

    public static final class ReservationStatus {
        public static final int PENDING = 1;
        public static final int CONFIRMED = 2;
        public static final int CANCELED = 3;
        public static final int COMPLETED = 4;
        public static final int REJECTED = 5;

        public static String getReadableStatus(int status) {
            switch (status) {
                case 1:
                    return "Pending";
                case 2:
                    return "Confirmed";
                case 3:
                    return "Canceled";
                case 4:
                    return "Completed";
                case 5:
                    return "Rejected";
                default:
                    return "Unknown";
            }
        }
    }

    public static final class ServerEndPoints {
        public static final String GET_RESTAURANTS = serverUrl + "/restaurants/list";
        public static final String LOGIN = serverUrl + "/login";
    }

    public static final class ErrorCodes {
        public static final int SERVER_UNREACHEABLE = 1004;
        public static final int OPERATION_ERROR = 3;
        public static final int EMAIL_PASS_ERROR = 2001;
    }

    public static final int HTTP_CONNECTION_TIMEOUT_SHORT = 15000;
    public static final int HTTP_CONNECTION_TIMEOUT_LONG = 60000;
}
