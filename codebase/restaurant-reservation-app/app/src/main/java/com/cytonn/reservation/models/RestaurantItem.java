package com.cytonn.reservation.models;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by F'rdaws on 30-Sep-17.
 */

public class RestaurantItem implements ClusterItem{

    Long id;
    String name;
    LatLng location;
    String imageUrl;

    public RestaurantItem(Long id, String name, LatLng location) {
        this.id = id;
        this.name = name;
        this.location = location;
    }

    public RestaurantItem(Long id, String name, LatLng location, String imageUrl) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.imageUrl = imageUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LatLng getLocation() {
        return location;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public LatLng getPosition() {
        return location;
    }
}
