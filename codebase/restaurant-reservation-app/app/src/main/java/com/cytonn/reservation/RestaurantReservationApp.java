package com.cytonn.reservation;

import android.app.Application;
import android.content.Context;

import com.cytonn.reservation.db.ReservationAppSQLiteOpenHelper;
import com.cytonn.reservation.db.dao.DaoSession;
import com.cytonn.reservation.utils.AccountUtils;

/**
 * Created by Firdaws on 29-Sep-17.
 */
public class RestaurantReservationApp extends Application {

    private static RestaurantReservationApp instance;
    private AccountUtils accountUtils;

    @Override
    public void onCreate() {
        super.onCreate();
        getDaoSessionInstance();
    }

    public RestaurantReservationApp() {
        super();
        instance = this;
    }

    public static RestaurantReservationApp getInstance() {
        return instance;
    }

    public static void setInstance(RestaurantReservationApp instance) {
        RestaurantReservationApp.instance = instance;
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    public static DaoSession getDaoSessionInstance() {
        return ReservationAppSQLiteOpenHelper.getDaoSession();
    }

    public AccountUtils getAccountUtils() {
        if(accountUtils == null)
            accountUtils = new AccountUtils();
        return accountUtils;
    }
}
