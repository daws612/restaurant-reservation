package com.cytonn.reservation.utils;

import android.util.Log;

import org.apache.http.NameValuePair;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

public class CustomHttpHandler {

    @SuppressWarnings("deprecation")
    public static String addParameters(List<NameValuePair> params) {
        String queryString = "?";
        try {
            for (NameValuePair nameValuePair : params) {
                queryString = queryString.concat(nameValuePair.getName() + "="
                        + URLEncoder.encode(nameValuePair.getValue(), "UTF-8")
                        + "&");
            }

            if (queryString.charAt(queryString.length() - 1) == '&') {
                queryString = queryString
                        .substring(0, queryString.length() - 1);
            }
        } catch (UnsupportedEncodingException e) {
            Log.e("CustomHttpHandler", e.getMessage(), e);
        }
        return queryString;
    }

}
