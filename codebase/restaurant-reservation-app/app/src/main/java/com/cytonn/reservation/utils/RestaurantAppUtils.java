package com.cytonn.reservation.utils;

import com.cytonn.reservation.RestaurantReservationApp;
import com.cytonn.reservation.shared.Constants;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by F'rdaws on 02-Oct-17.
 */

public class RestaurantAppUtils {
    public final String TAG = this.getClass().getSimpleName();

    static AccountUtils accountUtils = RestaurantReservationApp.getInstance().getAccountUtils();

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static boolean isCustomer() {
        return accountUtils.hasCurrentUser() && accountUtils.getCurrentUser().getUserType() == Constants.UserType.CUSTOMER;
    }

    public static boolean isOwner() {
        return accountUtils.hasCurrentUser() && accountUtils.getCurrentUser().getUserType() == Constants.UserType.OWNER;
    }

    public static long getCurrentUserId() {
        if (accountUtils.hasCurrentUser())
            return accountUtils.currentLoggedInUserId();
        else
            return -1;
    }
}
