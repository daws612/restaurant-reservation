package com.cytonn.reservation.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NavUtils;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cytonn.reservation.R;
import com.cytonn.reservation.db.models.Restaurant;
import com.cytonn.reservation.shared.Constants;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Date;

public class AddRestaurantActivity extends BaseActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener {
    private final String TAG = this.getClass().getSimpleName();

    public static String RESTAURANT_ID = "restId";

    private EditText nameText, addressText, phoneText, capacityText, descriptionText;
    private TextInputLayout nameWrapper, addressWrapper, phoneWrapper, capacityWrapper, descriptionWrapper;
    private FloatingActionButton saveRestaurantBtn;
    protected ImageView pic1, pic2, pic3;
    protected TextView takepic1, takepic2, takepic3, takelocation;
    LatLng selectedLocation;

    protected GoogleMap googleMap;

    protected int PLACE_PICKER_REQUEST = 1;
    Long restId;
    Restaurant restaurant = new Restaurant();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_restaurant);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        restId = getIntent().getLongExtra(RESTAURANT_ID, -1);

        nameWrapper = (TextInputLayout) findViewById(R.id.name_wrapper);
        nameText = (EditText) findViewById(R.id.rest_name);

        addressWrapper = (TextInputLayout) findViewById(R.id.address_wrapper);
        addressText = (EditText) findViewById(R.id.rest_address);

        phoneWrapper = (TextInputLayout) findViewById(R.id.phone_wrapper);
        phoneText = (EditText) findViewById(R.id.rest_phone);

        capacityWrapper = (TextInputLayout) findViewById(R.id.capacity_wrapper);
        capacityText = (EditText) findViewById(R.id.rest_capacity);

        descriptionWrapper = (TextInputLayout) findViewById(R.id.description_wrapper);
        descriptionText = (EditText) findViewById(R.id.description);

        saveRestaurantBtn = (FloatingActionButton) findViewById(R.id.save_rest);

        pic1 = (ImageView) findViewById(R.id.pic1_container);
        pic2 = (ImageView) findViewById(R.id.pic2_container);
        pic3 = (ImageView) findViewById(R.id.pic3_container);

        takepic1 = (TextView) findViewById(R.id.take_pic1_picture);
        takepic2 = (TextView) findViewById(R.id.take_pic2_picture);
        takepic3 = (TextView) findViewById(R.id.take_pic3_picture);

        takelocation = (TextView) findViewById(R.id.take_rest_location);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.rest_location_map);

        mapFragment.getMapAsync(this);

        setEventListeners();
        if(restId > -1) {
            restaurant = daoSession.getRestaurantDao().load(restId);
            populateFields();
        }
    }

    private void populateFields() {
        nameText.setText(restaurant.getName());
        addressText.setText(restaurant.getAddress());
        phoneText.setText(restaurant.getPhoneNumber());
        capacityText.setText(restaurant.getCapacity().toString());
        descriptionText.setText(restaurant.getDescription());
        selectedLocation = new LatLng(restaurant.getLatitude(), restaurant.getLongitude());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap theMap) {
        googleMap = theMap;

        googleMap.setPadding(0, 280, 0, 70);

        googleMap.setOnMapClickListener(this);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
        }

        moveAndZoomLocation(selectedLocation);

    }

    @Override
    public void onMapClick(LatLng latLng) {
        openPlacePicker();
    }

    protected void openPlacePicker() {
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == PLACE_PICKER_REQUEST) {
                Place place = PlacePicker.getPlace(data, this);
                googleMap.clear();
                selectedLocation = place.getLatLng();
                moveAndZoomLocation(place.getLatLng());
            }

        }
    }

    private void moveAndZoomLocation(LatLng selectedLocation) {
        if(selectedLocation == null)
            return;

        googleMap.addMarker(new MarkerOptions().position(selectedLocation).title(getString(R.string.title_activity_add_restaurant))
                .icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(selectedLocation, Constants.DEFAULT_ZOOM));
    }

    public void setEventListeners() {
        saveRestaurantBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                validationAndSave();

            }
        });
    }

    private void validationAndSave() {
        closeSoftKeyboard();
        saveRestaurantBtn.setEnabled(false);

        Animation shake = AnimationUtils.loadAnimation(this, R.anim.shake);

        String name = nameText.getText().toString();
        String address = addressText.getText().toString();
        String phone = phoneText.getText().toString();
        String capacity = capacityText.getText().toString();
        String description = descriptionText.getText().toString();


        boolean validated = true;

        if (name.isEmpty()) {
            nameWrapper.setError(getResources().getString(R.string.error_field_required));
            nameWrapper.startAnimation(shake);
            validated = false;
        } else {
            nameWrapper.setError(null);
        }

        if (address.isEmpty()) {
            addressWrapper.setError(getResources().getString(R.string.error_field_required));
            addressWrapper.startAnimation(shake);
            validated = false;
        } else {
            addressWrapper.setError(null);
        }

        if (phone.isEmpty()) {
            phoneWrapper.setError(getResources().getString(R.string.error_field_required));
            phoneWrapper.startAnimation(shake);
            validated = false;
        } else {
            if(!accountUtils.providedPhoneNumberHasCorrectFormat(phone)){
                phoneWrapper.setError(getResources().getString(R.string.error_field_required));
                phoneWrapper.startAnimation(shake);
                validated = false;
            }
            phoneWrapper.setError(null);
        }

        Integer capacityValue = null;
        if (capacity.isEmpty()) {
            capacityWrapper.setError(getResources().getString(R.string.error_field_required));
            capacityWrapper.startAnimation(shake);
            validated = false;
        } else {
            capacityValue = Integer.valueOf(capacity);
            capacityWrapper.setError(null);
        }

        if (description.isEmpty() || description.length() < 10) {
            descriptionWrapper.setError(getResources().getString(R.string.error_field_required));
            descriptionWrapper.startAnimation(shake);
            validated = false;
        } else {
            descriptionWrapper.setError(null);
        }

        //validate pictures here

        if (selectedLocation == null) {
            takelocation.setTextColor(getResources().getColor(android.R.color.holo_red_light));
            takelocation.startAnimation(shake);
            validated = false;
        } else {
            takelocation.setTextColor(getResources().getColor(android.R.color.darker_gray));
        }

        if (validated) {
            try {
                if(restId > -1) {
                    restaurant = daoSession.getRestaurantDao().load(restId);
                }

                restaurant.setUserId(accountUtils.getCurrentUser().getId());
                restaurant.setName(name);
                restaurant.setAddress(address);
                restaurant.setPhoneNumber(phone);
                restaurant.setCapacity(capacityValue);
                restaurant.setDescription(description);
                restaurant.setLatitude(selectedLocation.latitude);
                restaurant.setLongitude(selectedLocation.longitude);
                restaurant.setUserId(accountUtils.getCurrentUser().getId());

                if(restId > -1) {
                    restaurant.setUpdatedAt(new Date());
                    daoSession.getRestaurantDao().update(restaurant);
                } else {
                    restaurant.setCreatedAt(new Date());
                    daoSession.getRestaurantDao().insert(restaurant);
                }

                getIntent().removeExtra(RESTAURANT_ID);
                finish();

                //redirect to restaurants page
                startActivity(new Intent(this, RestaurantsActivity.class));
                finish();

            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
                saveRestaurantBtn.setEnabled(true);
            }
        } else {
            Toast.makeText(AddRestaurantActivity.this, "Unable to proceed. Please correct the highlighted fields and re-submit", Toast.LENGTH_LONG).show();
            saveRestaurantBtn.setEnabled(true);
        }

    }

}
