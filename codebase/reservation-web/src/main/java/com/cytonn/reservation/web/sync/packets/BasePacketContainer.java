package com.cytonn.reservation.web.sync.packets;

import java.util.List;

public class BasePacketContainer {

    public int responseCode = 2;// default unreachable
    public List<SyncError> syncErrors;
    public AuthenticationToken authenticationToken;

    public UserPacket user; // This value is only populated when the server is responding to a Login request

}
