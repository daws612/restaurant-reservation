package com.cytonn.reservation.web.contollers;

import org.springframework.beans.factory.annotation.Autowired;

import com.alliedcommerce.transport.sync.packets.ConsumerPacketContainer;
import com.alliedcommerce.transport.web.controllers.sync.utils.SyncRequestHandler;

public class BaseController {

	@Autowired
    SyncRequestHandler<ConsumerPacketContainer> syncRequestHandler;
}
