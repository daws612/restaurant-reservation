package com.cytonn.reservation.web.sync.packets;

import org.msgpack.annotation.Message;

/**
 * Created by F'rdaws on 04-Oct-17.
 */

@Message
public class ImagePacket {

    public Long masterId;

    public Long restaurantId;

    public String imageUrl;

    public Integer width;

    public Integer height;
}
