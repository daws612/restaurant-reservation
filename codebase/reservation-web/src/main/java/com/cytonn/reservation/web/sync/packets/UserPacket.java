package com.cytonn.reservation.web.sync.packets;

import org.msgpack.annotation.Message;

@Message
public class UserPacket {

    public Long masterId;

    public String email;

    public String firstName;

    public String lastName;

    public String password;

    public String mainPhone;
}