package com.cytonn.reservation.web.sync.msgpack;

import java.io.IOException;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.msgpack.MessageTypeException;
import org.msgpack.packer.Packer;
import org.msgpack.template.AbstractTemplate;
import org.msgpack.template.FieldOption;
import org.msgpack.template.Template;
import org.msgpack.template.TemplateRegistry;
import org.msgpack.template.builder.AbstractTemplateBuilder;
import org.msgpack.template.builder.DefaultFieldEntry;
import org.msgpack.template.builder.FieldEntry;
import org.msgpack.template.builder.TemplateBuildException;
import org.msgpack.unpacker.Unpacker;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class TransportTemplateBuilder extends AbstractTemplateBuilder {

    static final Log logger = LogFactory.getLog(TransportMessagePack.class);

    protected class ReflectionClassTemplate<T> extends AbstractTemplate<T> {

        protected Class<T> targetClass;
        int targetApiVersion;// default 0 accept all

        private volatile Map<String, ReflectionClassTemplate<?>> propertyTemplates;

        private volatile Template<?> extensionTemplate;

        ReflectionClassTemplate(int targetApi, Class<T> targetClass, Class<?> listClass) {

            this.targetClass = targetClass;
            this.targetApiVersion = targetApi;

            if (isPrimitiveOrPrimitiveWrapper(targetClass)) {
                // this.propertyTemplates = new HashMap();
                extensionTemplate = registry.lookup(targetClass);

            } else if (Map.class.isAssignableFrom(targetClass)) {
                this.propertyTemplates = new HashMap();
                // ParameterizedType listListType = (ParameterizedType) targetClass.getGenericInterfaces();
                // Class<?> listClass = (Class<?>) targetClass.getGenericInterfaces()[1];
                // Class<?> listClass = (Class<?>) ((ParameterizedType) fieldEntry.getGenericType()).getActualTypeArguments()[1];
                propertyTemplates.put("0", new ReflectionClassTemplate<T>(targetApiVersion, (Class<T>) listClass, null));

            } else if (List.class.isAssignableFrom(targetClass)) {
                this.propertyTemplates = new HashMap();
                // ParameterizedType listListType = (ParameterizedType) targetClass.getGenericInterfaces();
                // Class<?> listClass = (Class<?>) targetClass.getGenericInterfaces()[0];
                // Class<?> listClass = (Class<?>) ((ParameterizedType) fieldEntry.getGenericType()).getActualTypeArguments()[0];
                propertyTemplates.put("0", new ReflectionClassTemplate<T>(targetApiVersion, (Class<T>) listClass, null));

            } else {
                registry.register(targetClass, this);

                this.propertyTemplates = new HashMap();
                // custom object
                FieldOption fieldOption = getFieldOption(targetClass);
                FieldEntry[] entries = toFieldEntries(targetClass, fieldOption);
                // List<FieldEntry> versionedFields = Arrays.asList(entries);
                List<FieldEntry> versionedFields = new ArrayList<FieldEntry>(Arrays.asList(entries));
                // FieldEntry[] versionedEntries = entries.toArray(new FieldEntry[entries.size()]);

                for (FieldEntry entry : versionedFields) {

                    // prevent infinite loop when class A has B and B has A
                    Template t = registry.lookup(entry.getType());
                    if (t != null && t instanceof ReflectionClassTemplate) {
                        propertyTemplates.put(entry.getName(), (ReflectionClassTemplate<?>) t);
                    } else {
                        Class<?> childClass = null;
                        if (Map.class.isAssignableFrom(entry.getType())) {
                            // ParameterizedType listListType = (ParameterizedType) targetClass.getGenericInterfaces();
                            // Class<?> listClass = (Class<?>) targetClass.getGenericInterfaces()[1];
                            childClass = (Class<?>) ((ParameterizedType) entry.getGenericType()).getActualTypeArguments()[1];

                        } else if (List.class.isAssignableFrom(entry.getType())) {
                            // ParameterizedType listListType = (ParameterizedType) targetClass.getGenericInterfaces();
                            // Class<?> listClass = (Class<?>) targetClass.getGenericInterfaces()[0];
                            childClass = (Class<?>) ((ParameterizedType) entry.getGenericType()).getActualTypeArguments()[0];

                        }
                        propertyTemplates.put(entry.getName(), new ReflectionClassTemplate<T>(targetApiVersion, (Class<T>) entry.getType(), childClass));
                    }

                }
            }
        }

        @Override
        public void write(Packer packer, T target, boolean required) throws IOException {
            if (target == null) {
                if (required) {
                    throw new MessageTypeException("attempted to write null");
                }
                packer.writeNil();
                return;
            }
            int count = 0;
            try {

                // Identify the entries to serialize.
                Object[] entriesToWrite = new Object[propertyTemplates.entrySet().size() * 2]; // keys and values interleaved: k0, v0, k1, v1, ...

                for (Entry<String, ReflectionClassTemplate<?>> entry : propertyTemplates.entrySet()) {

                    String key = entry.getKey();
                    Field field = target.getClass().getField(key);
                    field.setAccessible(true);
                    Object value = field.get(target);
                    // This entry differs from the default value, so we will write this to the stream.
                    entriesToWrite[count++] = key;
                    entriesToWrite[count++] = value;
                }

                // Write the entries to the MessagePack stream.
                packer.writeMapBegin(count >> 1);
                for (int i = 0; i < count; i++) {

                    String key = (String) entriesToWrite[i];

                    packer.write(key);
                    Object val = entriesToWrite[++i];
                    if (val != null && List.class.isAssignableFrom(val.getClass())) {
                        ReflectionClassTemplate<?> t = propertyTemplates.get(key).propertyTemplates.get("0");
                        int c = ((List) val).size();

                        packer.writeArrayBegin(c);
                        for (int j = 0; j < c; j++) {
                            t.internalWrite(packer, ((List) val).get(j));
                        }
                        packer.writeArrayEnd();
                    } else if (val != null && Map.class.isAssignableFrom(val.getClass())) {
                        ReflectionClassTemplate<?> t = propertyTemplates.get(key).propertyTemplates.get("0");
                        HashMap<Object, Object> map = new HashMap<Object, Object>((Map) val);
                        int c = ((Map) val).size();
                        packer.writeMapBegin(c);
                        for (Entry entry : map.entrySet()) {
                            Object k = entry.getKey();
                            packer.write(k);
                            t.internalWrite(packer, entry.getValue());
                        }

                        packer.writeMapEnd();
                    } else if (val != null && !isPrimitiveOrPrimitiveWrapper(val.getClass())) { // custom class
                        ReflectionClassTemplate<?> t = propertyTemplates.get(key);
                        t.internalWrite(packer, val);

                    } else {
                        packer.write(val);
                    }

                }
                packer.writeMapEnd();

            } catch (StackOverflowError e) {
                throw e;
            } catch (IOException e) {
                throw e;
            } catch (Exception e) {
                throw new MessageTypeException(e);
            }
        }

        public void internalWrite(Packer packer, Object target) throws IOException {
            write(packer, (T) target, false);
        }

        @Override
        public T read(Unpacker unpacker, T to, boolean required) throws IOException {
            if (!required && unpacker.trySkipNil()) {
                return null;
            }
            try {

                final int size = unpacker.readMapBegin();
                for (int i = 0; i < size; i++) {
                    String key = unpacker.readString();
                    Object value;

                    ReflectionClassTemplate<?> propertyTemplate = propertyTemplates.get(key);
                    if (propertyTemplate == null) {// should never happen
                        logger.debug(String.format("missing template for field: %s silently skipping field. check api version", key));
                        unpacker.readValue();// waste it silently
                        continue;
                    }
                    if (propertyTemplate.extensionTemplate != null) {
                        value = unpacker.read(propertyTemplate.extensionTemplate);
                    } else {

                        try {

                            if (List.class.isAssignableFrom(propertyTemplate.targetClass)) {
                                ArrayList arrayList = new ArrayList();

                                // Value v = unpacker.readValue();
                                ReflectionClassTemplate<?> childpropertyTemplate = propertyTemplate.propertyTemplates.get("0");
                                if (!unpacker.trySkipNil()) {
                                    // new Converter(v).read(tList(TString));
                                    final int sizec = unpacker.readArrayBegin();// unpacker.readArrayBegin();
                                    for (int j = 0; j < sizec; j++) {

                                        Object e = childpropertyTemplate.read(unpacker, null);
                                        arrayList.add(e);
                                    }
                                    unpacker.readArrayEnd();// unpacker.readArrayEnd();
                                    value = arrayList;
                                } else {
                                    value = null;
                                }

                            } else {
                                value = propertyTemplate.read(unpacker, null, false);
                            }

                        } catch (MessageTypeException e) {
                            throw new MessageTypeException(String.format("Exception reading value for key '%s'", key), e);
                        }

                    }

                    if (to == null) {

                        to = targetClass.newInstance();

                    }
                    Field field = to.getClass().getField(key);
                    field.setAccessible(true);
                    // builder.put(key, value);
                    field.set(to, value);

                }
                unpacker.readMapEnd();

                return to;

            } catch (Exception e) {
                throw new MessageTypeException(e);
            }
        }
    }

    public TransportTemplateBuilder(TemplateRegistry registry) {
        super(registry);
    }

    @Override
    public boolean matchType(Type targetType, boolean hasAnnotation) {
        Class<?> targetClass = (Class<?>) targetType;
        boolean matched = matchAtClassTemplateBuilder(targetClass, hasAnnotation);
        if (matched) {
            logger.info("matched type: " + targetClass.getName());
        }
        return matched;
    }

    public <T> Template<T> buildRokTemplate(int targetApiVersion, final Type targetType) throws TemplateBuildException {
        @SuppressWarnings("unchecked")
        Class<T> targetClass = (Class<T>) targetType;
        checkClassValidation(targetClass);

        return new ReflectionClassTemplate<T>(targetApiVersion, targetClass, null);

    }

    @Override
    protected <T> Template<T> buildTemplate(Class<T> targetClass, FieldEntry[] arg1) {
        return new ReflectionClassTemplate<T>(1, targetClass, null);
    }

    private static boolean isPrimitiveOrPrimitiveWrapper(Class c) {
        return (c.isPrimitive() || c.getName().startsWith("java.lang") || c.getName().startsWith("java.math") || c.getName().startsWith("java.util.Date"));

    }
}
