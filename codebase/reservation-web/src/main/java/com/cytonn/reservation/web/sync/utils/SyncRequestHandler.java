package com.cytonn.reservation.web.sync.utils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.AbstractPlatformTransactionManager;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.cytonn.reservation.web.sync.packets.BasePacketContainer;
import com.cytonn.reservation.web.sync.packets.SyncError;

@Component
public class SyncRequestHandler<T extends BasePacketContainer> {

    private T requestContainer;
    private T responseContainer;
    Class<T> clazzContainer;

    static int TRANSACTION_TIMEOUT = 10;

    @Autowired(required = true)
    protected MessageResolver messageResolver;
    // GeopingService geopingService;
    final Log logger = LogFactory.getLog(getClass());

    public void setPayload(Class<T> clazzContainer, byte[] payload) {
        try {
            this.clazzContainer = clazzContainer;
            responseContainer = this.clazzContainer.newInstance();
            responseContainer.syncErrors = new ArrayList<SyncError>();

            this.setSyncRecordCount(0);

            // unpack

            requestContainer = TransportMessagePack.unpack(payload, clazzContainer);
        } catch (Exception e) {
            logger.error("Unable to unpack or create clazzContainer.newInstance()", e);

            SyncError syncError = new SyncError();
            syncError.errorMessage = messageResolver.getErrorMessage(SyncErrors.UNABLE_TO_UNPACK);
            syncError.errorCode = Constants.TransportSync.SyncErrors.UNABLE_TO_UNPACK;
            responseContainer.syncErrors.add(syncError);
        }

    }

    public int recordsToFetch() {

        if (requestContainer == null) {
            throw new RuntimeException("how can requestContainer be null?");
        }
        if (responseContainer == null) {
            throw new RuntimeException("how can responseContainer be null?");
        }
        int records = requestContainer.syncPageLimit - getSyncRecordCount();
        if (records < 1) {
            responseContainer.doFollowUpSync = true;
        }
        return records;
    }

    public boolean valid() {

        if (responseContainer.syncErrors.size() > 0) {// failed at unpacking probably
            return false;
        }
        if (requestContainer == null && responseContainer.syncErrors.size() < 1) {// if could not unpack don't add a new error entry
            logger.debug("Empty Packet");

            SyncError syncError = new SyncError();
            syncError.errorMessage = messageResolver.getErrorMessage(SyncErrors.EMPTY_PACKET);
            syncError.errorCode = Constants.TransportSync.SyncErrors.EMPTY_PACKET;
            responseContainer.syncErrors.add(syncError);

            return false;
        }
        boolean valid = isValidPacketContainer();
        if (!valid) {
            SyncError syncError = new SyncError();
            syncError.errorMessage = messageResolver.getErrorMessage(SyncErrors.INVALID_PACKET);
            syncError.errorCode = Constants.TransportSync.SyncErrors.INVALID_PACKET;
            responseContainer.syncErrors.add(syncError);
            logger.debug(syncError.errorMessage);
        }
        return valid;
    }

    /**
     * Authenticate calls .valid()
     * 
     * @param ipAddress
     * @return
     */
	public boolean authenticateUser(String ipAddress) {
        // authenticate packet (http://confluence.alliedtechnique.net/display/ROKMOBILE/Data+Synchronization+Service)

        if (!valid())
            return false;
        if (requestContainer.authenticationToken == null || requestContainer.authenticationToken.userName == null
                || requestContainer.authenticationToken.userPassword == null) {
            return false;
        }

        return true;
    }

    public T getRequestContainer() {
        return requestContainer;
    }

    public T getResponseContainer() {
        return responseContainer;
    }

    public byte[] getPayLoad() {
        // Add the correct response code before sending back the packet
        if (responseContainer.syncErrors != null && responseContainer.syncErrors.size() > 0) {
            responseContainer.responseCode = 1;
        } else {
            responseContainer.responseCode = 0;
        }
        int targetVersion = NumberUtils.toInt(requestContainer == null ? null : requestContainer.apiVersion, 0);
        byte[] payload = TransportMessagePack.pack(targetVersion, responseContainer);

        // payload = null ;
        return payload;
    }

    public Class<T> getClazzContainer() {
        return clazzContainer;
    }

    public void setClazzContainer(Class<T> clazzContainer) {
        this.clazzContainer = clazzContainer;
    }

}
