package com.cytonn.reservation.web.services;

import com.cytonn.reservation.data.domain.User;


public interface UserService {

	public User getUserById();
}
