package com.cytonn.reservation.web.sync.packets;

import org.msgpack.annotation.Message;

import java.util.Date;

/**
 * Created by F'rdaws on 04-Oct-17.
 */

@Message
public class ReservationPacket {

    public Long masterId;

    public Long userId;

    public Long restaurantId;

    public String note;

    public Integer capacity;

    public boolean isDeleted;

    public Date reservationAt;

    public Short status;
}
