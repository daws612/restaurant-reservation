/*package com.cytonn.reservation.web.auth;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.util.UrlPathHelper;

import com.alliedcommerce.shopping.auth.ClientAuthenticationToken;
import com.alliedcommerce.shopping.auth.CustomUserTokenMatcher;
import com.alliedcommerce.shopping.models.AuthenticationToken;
import com.alliedcommerce.shopping.services.CustomUserDetailsService;
import com.alliedcommerce.shopping.services.CustomUserDetailsServiceImpl;
import com.alliedcommerce.shopping.util.Constants;
import com.alliedcommerce.shopping.util.Constants.ResponseCodes;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
public class SecurityContext extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.httpBasic().disable();
    }

    @Bean
    public ShiroFilterFactoryBean shiroFilter() {
        ShiroFilterFactoryBean shiroFilter = new ShiroFilterFactoryBean();

        shiroFilter.setSecurityManager(securityManager());
        Map<String, Filter> filters = new HashMap<String, Filter>();

        filters.put("authFilter", authFilter());
        shiroFilter.setFilters(filters);
        return shiroFilter;
    }

    @Bean
    public DefaultWebSecurityManager securityManager() {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();

        securityManager.setRealm((AuthorizingRealm) customUserDetailsService());// realm());

        return securityManager;
    }

    @Bean
    @DependsOn("lifecycleBeanPostProcessor")
    public CustomUserDetailsService customUserDetailsService() {

        CustomUserDetailsService customUserDetailsService = new CustomUserDetailsServiceImpl();
        ((AuthorizingRealm) customUserDetailsService).setCredentialsMatcher(customUserTokenMatcher());
        return customUserDetailsService;
    }

    @Bean
    public CustomUserTokenMatcher customUserTokenMatcher() {
        return new CustomUserTokenMatcher();
    }

    @Bean
    public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }

    @Bean
    public AuthFilter authFilter() {
        return new AuthFilter();
    }

    public class AuthFilter implements javax.servlet.Filter {

        Log log = LogFactory.getLog(this.getClass());

        public static final String X_AUTHORIZATION = "X-Authorization";

        @Override
        public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {

            HttpServletResponseWrapper resp = new HttpServletResponseWrapper((HttpServletResponse) res) {
                @Override
                public void setStatus(int sc) {
                    super.setStatus(sc);
                    handleStatus(sc);
                }

                @Override
                @SuppressWarnings("deprecation")
                public void setStatus(int sc, String sm) {
                    super.setStatus(sc, sm);
                    handleStatus(sc);
                }

                @Override
                public void sendError(int sc, String msg) throws IOException {
                    super.sendError(sc, msg);
                    handleStatus(sc);
                }

                @Override
                public void sendError(int sc) throws IOException {
                    super.sendError(sc);
                    handleStatus(sc);
                }

                private void handleStatus(int code) {
                    // if(code == 404)
                    // addHeader("dummy-header", "dummy-value");
                }
            };

            try {
                HttpServletRequest request = (HttpServletRequest) req;
                String auth = request.getHeader(X_AUTHORIZATION);

                String resourcePath = new UrlPathHelper().getPathWithinApplication(request);
                // do auth
                if (!skipAuthenticate(resourcePath)) {
                    String ipAddress = request.getHeader("X-FORWARDED-FOR");
                    if (ipAddress == null) {
                        ipAddress = request.getRemoteAddr();
                    }
                    ObjectMapper objectMapper = new ObjectMapper();
                    AuthenticationToken authenticationToken = objectMapper.readValue(auth, AuthenticationToken.class);
                    performClientTokenLogin(authenticationToken.username, authenticationToken.clientUuid, authenticationToken.clientToken 
 * contains
 * clientToken
                                                                                                                                           ,
                            authenticationToken.clientId, ipAddress);
                }

            } catch (Exception e) {
                log.info("unauthorised user", e);

                resp.addHeader("content-type", "application/json");
                resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                resp.getWriter().print(String.format("{\"status\":\"%d\",\"message\": \"%s\"}", ResponseCodes.MISSING_USER_IDENTITY, "Missing User Identity"));
                resp.getWriter().flush();

                return;
            }
            chain.doFilter(req, res);
        }

        boolean skipAuthenticate(String resourcePath) {

            return resourcePath.equalsIgnoreCase(Constants.EndPoint.LOGIN);

        }

        @Override
        public void destroy() {
        }

        @Override
        public void init(FilterConfig arg0) throws ServletException {
        }

 *//**
 * Perform login when username, uuid, client access token and clientId are provided
 * 
 * @param username
 * @param uuid
 * @param clientToken
 * @param clientIdString
 * @param ipAddress
 */
/*
 * public synchronized void performClientTokenLogin(String username, String uuid, String clientToken, Long clientId, String ipAddress) throws
 * AuthenticationException, UnknownAccountException {
 * 
 * // check if parameters have been tampered with // String digestInput =
 * username.toLowerCase().concat(clientToken.toLowerCase()).concat("com.alliedcommerce.transport").concat(uuid.toUpperCase()); // String expectedSignature = new
 * Sha256Hash(digestInput, username, 1).toBase64();
 * 
 * // if (!expectedSignature.equals(signature)) { // throw new AuthenticationException("Authentication checksum failed"); // }
 * 
 * SecurityUtils.getSubject().login(new ClientAuthenticationToken(username, uuid, clientToken, clientId, ipAddress));
 * 
 * }
 * 
 * } }
 */