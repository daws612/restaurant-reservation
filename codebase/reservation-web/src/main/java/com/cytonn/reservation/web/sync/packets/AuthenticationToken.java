package com.cytonn.reservation.web.sync.packets;

import org.msgpack.annotation.Message;

@Message
public class AuthenticationToken {

    // required
    public String userName;

    public String userPassword;

    public String signature;

    public int signatureMethod;
}
