package com.cytonn.reservation.data;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.cytonn.reservation.data.domain.Restaurant;
import com.cytonn.reservation.data.domain.RestaurantExample;

public interface RestaurantMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table restaurants
     *
     * @mbggenerated Thu Oct 05 09:01:13 EAT 2017
     */
    int countByExample(RestaurantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table restaurants
     *
     * @mbggenerated Thu Oct 05 09:01:13 EAT 2017
     */
    int deleteByExample(RestaurantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table restaurants
     *
     * @mbggenerated Thu Oct 05 09:01:13 EAT 2017
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table restaurants
     *
     * @mbggenerated Thu Oct 05 09:01:13 EAT 2017
     */
    int insert(Restaurant record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table restaurants
     *
     * @mbggenerated Thu Oct 05 09:01:13 EAT 2017
     */
    int insertSelective(Restaurant record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table restaurants
     *
     * @mbggenerated Thu Oct 05 09:01:13 EAT 2017
     */
    List<Restaurant> selectByExample(RestaurantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table restaurants
     *
     * @mbggenerated Thu Oct 05 09:01:13 EAT 2017
     */
    Restaurant selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table restaurants
     *
     * @mbggenerated Thu Oct 05 09:01:13 EAT 2017
     */
    int updateByExampleSelective(@Param("record") Restaurant record, @Param("example") RestaurantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table restaurants
     *
     * @mbggenerated Thu Oct 05 09:01:13 EAT 2017
     */
    int updateByExample(@Param("record") Restaurant record, @Param("example") RestaurantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table restaurants
     *
     * @mbggenerated Thu Oct 05 09:01:13 EAT 2017
     */
    int updateByPrimaryKeySelective(Restaurant record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table restaurants
     *
     * @mbggenerated Thu Oct 05 09:01:13 EAT 2017
     */
    int updateByPrimaryKey(Restaurant record);
}