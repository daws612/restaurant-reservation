package com.cytonn.reservation.web.sync.packets;

import org.msgpack.annotation.Message;
import org.w3c.dom.Document;

@Message
public class RestaurantPacket {

    public Long masterId;

    public Long userId;

    public String restaurantName;

    public String mainPhone;

    public String address;

    public String description;

    public Double latitude;

    public Double longitude;

    public Integer capacity;

    public boolean isDeleted;
}