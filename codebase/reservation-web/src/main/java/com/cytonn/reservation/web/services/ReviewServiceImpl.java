package com.cytonn.reservation.web.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cytonn.reservation.data.ReviewMapper;
import com.cytonn.reservation.data.domain.Review;
import com.cytonn.reservation.data.domain.ReviewExample;

@Service
public class ReviewServiceImpl implements ReviewService {

	@Autowired
	ReviewMapper reviewMapper;

	@Override
	public List<Review> getReviewsOfRestaurant(Long restId) {

		List<Review> reviews = new ArrayList<Review>();

		try {
			ReviewExample reviewExample = new ReviewExample();
			reviewExample.createCriteria().andRestaurantIdEqualTo(restId);
			reviewExample.setOrderByClause("created_at desc");

			reviews = reviewMapper.selectByExample(reviewExample);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reviews;
	}

}
