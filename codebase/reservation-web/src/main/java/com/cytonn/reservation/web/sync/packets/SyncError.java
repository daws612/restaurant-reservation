package com.cytonn.reservation.web.sync.packets;

import org.msgpack.annotation.Message;

@Message
public class SyncError {

    public Integer errorCode;
    public String errorMessage;
    public int itemType;
    public Long masterId; /* Server master_id if available */
    public Integer localId;
}
