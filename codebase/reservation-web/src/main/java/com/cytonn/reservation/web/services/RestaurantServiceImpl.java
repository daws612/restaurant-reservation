package com.cytonn.reservation.web.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cytonn.reservation.data.RestaurantMapper;
import com.cytonn.reservation.data.domain.Restaurant;
import com.cytonn.reservation.data.domain.RestaurantExample;

@Service
public class RestaurantServiceImpl implements RestaurantService {

	@Autowired
	RestaurantMapper restaurantMapper;

	@Override
	public List<Restaurant> getAllRestaurants() {
		List<Restaurant> restList = new ArrayList<Restaurant>();
		
		RestaurantExample restaurantExample = new RestaurantExample();
		restaurantExample.createCriteria().andIsDeletedEqualTo(false);
		restaurantExample.setOrderByClause("created_at desc");
		
		restList = restaurantMapper.selectByExample(restaurantExample);

		return restList;
	}

}
