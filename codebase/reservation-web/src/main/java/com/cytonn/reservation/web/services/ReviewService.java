package com.cytonn.reservation.web.services;

import java.util.List;

import com.cytonn.reservation.data.domain.Review;


public interface ReviewService {

	public List<Review> getReviewsOfRestaurant(Long restId);

}
