package com.cytonn.reservation.web.services;

import java.util.List;

import com.cytonn.reservation.data.domain.Restaurant;


public interface RestaurantService {

	public List<Restaurant> getAllRestaurants();
}
