package com.cytonn.reservation.web.sync.packets;

import org.msgpack.annotation.Message;

/**
 * Created by F'rdaws on 04-Oct-17.
 */

@Message
public class ReviewPacket {

    public Long masterId;

    public Long userId;

    public Long restaurantId;

    public String reviewText;

    public Double rating;
}
