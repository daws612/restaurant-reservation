package com.cytonn.reservation.web.contollers;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cytonn.reservation.data.domain.Restaurant;
import com.cytonn.reservation.web.response.StandardJsonResponse;
import com.cytonn.reservation.web.response.StandardJsonResponseImpl;
import com.cytonn.reservation.web.services.RestaurantService;

@RestController
@RequestMapping(method = RequestMethod.GET, value = "/restaurants")
public class RestaurantController {

	@Autowired
	RestaurantService restaurantService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public @ResponseBody StandardJsonResponse list() throws Exception {

		StandardJsonResponse standardJsonResponse = new StandardJsonResponseImpl();

		final Log logger = LogFactory.getLog(RestaurantController.class);

		logger.info("Retrieving ");

		List<Restaurant> restList = restaurantService.getAllRestaurants();

		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("restList", restList);
		standardJsonResponse.setData(data);
		standardJsonResponse.setSuccess(true);

		return standardJsonResponse;

	}

}
