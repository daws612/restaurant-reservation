package com.cytonn.reservation.web.sync.packets;

import java.util.List;

/**
 * Created by Firdaws on 04-Oct-17.
 */
public class PacketContainer {
    public int responseCode = 2;// default unreachable
    public List<SyncError> syncErrors;
    public AuthenticationToken authenticationToken;

    public UserPacket userPacket;

    public ReservationPacket reservationPacket;

    public RestaurantPacket restaurantPacket;

    public ReviewPacket reviewPacket;

    public ImagePacket imagePacket;
}
