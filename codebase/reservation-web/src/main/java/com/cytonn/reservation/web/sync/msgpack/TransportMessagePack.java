package com.cytonn.reservation.web.sync.msgpack;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.msgpack.MessagePack;
import org.msgpack.packer.BufferPacker;
import org.msgpack.template.Template;
import org.msgpack.template.TemplateRegistry;
import org.msgpack.unpacker.Unpacker;

public class TransportMessagePack {

    static final Log logger = LogFactory.getLog(TransportMessagePack.class);
    static MessagePack msgpack = new MessagePack();

    static Map<String, Template> templateCache = new HashMap<String, Template>();

    static <T> Template<T> loadTemplate(int targetVersion, Class<T> type) {
        Template<T> tmpl;

        String key = targetVersion + type.getName();
        tmpl = templateCache.get(key);
        if (tmpl == null) {
            synchronized (templateCache) {
                tmpl = templateCache.get(key);
                if (tmpl == null) {
                    // create template and auto register
                    TemplateRegistry registry = new TemplateRegistry(null);
                    TransportTemplateBuilder builder = new TransportTemplateBuilder(registry);
                    tmpl = builder.buildRokTemplate(targetVersion, type);
                    registry.register(type, tmpl);
                    templateCache.remove(key);
                    templateCache.put(key, tmpl);
                }
            }
        }
        return tmpl;

    }

    /**
     * should throw to help detect an unpack error
     * 
     * @param payload
     * @param type
     * @return
     * @throws IOException
     */
    public static <T> T unpack(byte[] payload, Class<T> type) throws IOException {

        Template<T> tmpl = loadTemplate(0, type);
        Unpacker unpacker = msgpack.createBufferUnpacker(payload);
        unpacker.resetReadByteCount();
        // T ret = unpacker.read(tmpl);
        T ret = tmpl.read(unpacker, null);
        unpacker.close();
        return ret;

    }

    public static byte[] pack(int targetApiVersion, Object data) {
        try {

            Template tmpl = loadTemplate(targetApiVersion, data.getClass());
            BufferPacker packer = msgpack.createBufferPacker();
            tmpl.write(packer, data);
            byte[] bytes = packer.toByteArray();
            packer.close();
            return bytes;// msgpack.write(introspected);
        } catch (Exception e) {
            logger.debug("", e);
            return null;
        }
    }

}
