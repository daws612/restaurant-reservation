package com.cytonn.reservation.web;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@MapperScan(basePackages = "com.cytonn.reservation.data")
public class AppConfig {
	

	   @Bean
	   public DataSourceTransactionManager transactionManager(DataSource dataSource) {
	     return new DataSourceTransactionManager(dataSource);
	   }

	   @Bean
	   public SqlSessionFactory sqlSessionFactory(DataSource dataSource) throws Exception {
	     SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
	     sessionFactory.setDataSource(dataSource);
	     return sessionFactory.getObject();
	}
}
